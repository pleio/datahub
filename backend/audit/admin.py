from audit.models import Log
from django.contrib import admin


class LogAdmin(admin.ModelAdmin):
    list_display = (
        "date_created",
        "user",
        "class_name",
        "function",
        "message",
    )
    list_filter = (
        "date_created",
        "class_name",
        "user",
        "function",
    )

    ordering = ("-date_created",)
    search_fields = ("message", "user")


admin.site.register(Log, LogAdmin)
