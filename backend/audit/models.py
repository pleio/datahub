from core.models import DatahubUser
from django.db import models

LEVEL_CHOICES = (
    ("DEBUG", "DEBUG"),
    ("INFO", "INFO"),
    ("WARNING", "WARNING"),
    ("ERROR", "ERROR"),
    ("CRITICAL", "CRITICAL"),
)


class Log(models.Model):
    """Log entries"""

    date_created = models.DateTimeField(auto_now_add=True)
    level = models.CharField(max_length=256, choices=LEVEL_CHOICES, default="INFO")
    class_name = models.CharField(max_length=256, blank=True, null=True)
    function = models.CharField(max_length=256, blank=True, null=True)
    user = models.ForeignKey(
        DatahubUser, on_delete=models.SET_NULL, blank=True, null=True
    )
    message = models.TextField()

    def __str__(self):
        return f"{self.message}"
