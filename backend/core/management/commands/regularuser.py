from core.models import DatahubUser
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = "Downgrades the user to one without additional permissions"

    def add_arguments(self, parser):
        parser.add_argument("email", nargs="+", type=str)

    def handle(self, *args, **kwargs):
        email = kwargs["email"][0]
        try:
            user = DatahubUser.objects.get(email=email)

        except DatahubUser.DoesNotExist:
            raise CommandError(f"user with email {email} does not exist")

        user.is_superuser = False
        user.is_staff = False
        user.save()

        self.stdout.write(self.style.SUCCESS(f"Successfully downgraded {email}"))
