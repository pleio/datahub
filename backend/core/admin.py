from core.models import (
    Chapter,
    Classification,
    DatahubUser,
    File,
    FileExtension,
    Sensor,
    Study,
    Zone,
)
from django.contrib import admin


class FileAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "id",
        "date_created",
        "published",
        "processed",
        "study_published",
        "zone_published",
        "status_rename",
    )
    list_filter = (
        "date_created",
        "published",
        "processed",
        "sensor",
        "study",
        "classification",
        "zone",
        "status_rename",
    )
    ordering = ("-date_created",)
    search_fields = (
        "name",
        "date_created",
        "id",
        "sensor__name",
        "study__name",
        "classification__name",
        "zone__name",
    )

    def study_published(self, obj):
        if obj.study:
            return obj.study.published
        return None

    def zone_published(self, obj):
        if obj.zone:
            return obj.zone.published
        return None

    zone_published.boolean = True
    study_published.boolean = True


class ZoneAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "id",
        "date_created",
        "published",
    )


class StudyAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "id",
        "chapter__name",
        "date_created",
        "published",
    )


class ChapterAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "id",
        "date_created",
    )


class SensorAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "id",
        "date_created",
    )


class ClassificationAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "id",
        "date_created",
    )


class FileExtensionAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "id",
        "date_created",
    )

class DatahubUserAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "id",
        "name",
        "username",
        "date_joined",
        "is_active",
        "is_superadmin",
        "external_id",
        "picture",
    )


admin.site.register(Chapter, ChapterAdmin)
admin.site.register(Classification, ClassificationAdmin)
admin.site.register(DatahubUser, DatahubUserAdmin)
admin.site.register(File, FileAdmin)
admin.site.register(FileExtension, FileExtensionAdmin)
admin.site.register(Sensor, SensorAdmin)
admin.site.register(Study, StudyAdmin)
admin.site.register(Zone, ZoneAdmin)
