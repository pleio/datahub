from core.models import File
from core.serializers import FileDownloadGETSerializer
from django.core.cache import cache
from django.db.models import F, Q
from rest_framework.permissions import SAFE_METHODS, BasePermission, IsAdminUser


class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS


def get_permissions():
    """
    Grants the correct level of permissions.
    Anonymous users (not logged in) and regular users
    should both not be allowed to POST, PUT or DELETE
    and should not be able to GET unpublished or unprocessed data.
    Only admins have full CRUD permissions and the ability
    to view unpublished and unprocessed data.
    An admin is a user with 'is_staff' True.
    """
    return [IsAdminUser | ReadOnly]


def get_queryset_files(request):
    """
    UNPUBLISHED files should only be visible in GET requests
    for admin users that explicitly requested them.
    UNPROCESSED files should only be visible in GET requests
    for admin users that explicitly requested them.
    """
    qs = File.objects.prefetch_related(
        "classification", "sensor", "study", "extension", "zone", "study__chapter"
    ).filter(Q(is_deleted=False))

    if request.method != "GET":
        return qs.all()

    if request.user.is_staff:
        only_unprocessed = (
            request.query_params.get("only_unprocessed", "False").lower() == "true"
        )
        # Unprocessed implies unpublished, so let 'only_unprocessed=true' requests
        # assume also_unpublished=true
        if only_unprocessed:
            also_unpublished = True
        else:
            also_unpublished = (
                request.query_params.get("also_unpublished", "False").lower() == "true"
            )
        if not also_unpublished:
            qs = qs.filter(published=True)
        return qs.filter(processed=not only_unprocessed)

    # A GET request and no permissions to see unpublished files or studies or zones
    return qs.filter(
        Q(published=True)
        & (Q(study__published=True) | Q(study__isnull=True))
        & (Q(zone__published=True) | Q(zone__isnull=True))
    )


def file_download_data(file, session_key):
    serializer = FileDownloadGETSerializer()
    result = {
        "id": file.id,
        "download_command": serializer.get_download_command(file),
        "download_url": serializer.get_download_url(file),
    }
    # Avoid counting page refreshes
    if not combination_seen_before(session_key, file.id):
        file.nr_downloads = F("nr_downloads") + 1
        # Avoid updating the 'date_modified' field
        file.save(update_fields=["nr_downloads"])
    return result


def combination_seen_before(session_key, file_id):
    if session_key is None:
        return False

    combination = f"{session_key}{file_id}"
    if cache.get(combination):
        return True

    cache.set(combination, True, timeout=86400)
    return False
