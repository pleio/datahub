from core.views import (
    ChapterDetail,
    ChapterList,
    ClassificationList,
    ExtensionList,
    FileDetail,
    FileDownload,
    FileList,
    FilesDownload,
    HealthView,
    SensorList,
    StudyDetail,
    StudyList,
    TotalsView,
    UserInfo,
    ZoneDetail,
    ZoneList,
    logout_view,
)
from django.urls import path

# API routes
urlpatterns = [
    path("health/", HealthView.as_view(), name="api-v1-health"),
    path("logout/", logout_view, name="api-v1-logout"),
    path("users/me/", UserInfo.as_view(), name="api-v1-userinfo"),
    path("totals/", TotalsView.as_view(), name="api-v1-totals"),
    path("sensors/", SensorList.as_view(), name="api-v1-sensors"),
    path("chapters/", ChapterList.as_view(), name="api-v1-chapters"),
    path("chapters/<int:pk>/", ChapterDetail.as_view(), name="api-v1-chapter"),
    path("files/", FileList.as_view(), name="api-v1-files"),
    path("files/download/", FilesDownload.as_view(), name="api-v1-files-download"),
    path("files/<int:pk>/", FileDetail.as_view(), name="api-v1-file"),
    path(
        "files/<int:pk>/download/", FileDownload.as_view(), name="api-v1-file-download"
    ),
    path("studies/", StudyList.as_view(), name="api-v1-studies"),
    path("studies/<int:pk>/", StudyDetail.as_view(), name="api-v1-study"),
    path("zones/", ZoneList.as_view(), name="api-v1-zones"),
    path("zones/<int:pk>/", ZoneDetail.as_view(), name="api-v1-zone"),
    path(
        "classifications/", ClassificationList.as_view(), name="api-v1-classifications"
    ),
    path("extensions/", ExtensionList.as_view(), name="api-v1-extensions"),
]
