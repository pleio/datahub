from core.azure_storage import Blob
from core.models import (
    Chapter,
    Classification,
    File,
    FileExtension,
    Sensor,
    Study,
    Zone,
)
from rest_framework import serializers


def RelatedFieldSerializer(input_model):
    """
    Factory method to produce a customized serializer for
    a related field
    """

    class ResultSerializer(serializers.ModelSerializer):
        class Meta:
            model = input_model
            fields = (
                "id",
                "name",
            )

    return ResultSerializer()


class SensorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sensor
        fields = "__all__"


class ChapterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chapter
        fields = "__all__"


class FileGETSerializer(serializers.ModelSerializer):
    classification = RelatedFieldSerializer(Classification)
    sensor = RelatedFieldSerializer(Sensor)
    study = RelatedFieldSerializer(Study)
    extension = RelatedFieldSerializer(FileExtension)
    chapter = serializers.SerializerMethodField()
    zone = RelatedFieldSerializer(Zone)

    def get_chapter(self, obj):
        """Returns the name and id of a chapter of a study of a file"""
        study = obj.study
        if study:
            chapter = study.chapter
            if chapter:
                return {"id": chapter.id, "name": chapter.name}
        return None

    class Meta:
        model = File
        extra_fields = ("chapter", "zone")
        exclude = ("url", "is_deleted")


class FileDownloadGETSerializer(serializers.ModelSerializer):
    download_command = serializers.SerializerMethodField()
    download_url = serializers.SerializerMethodField()

    def get_download_command(self, file):
        return Blob(file).create_download_command()

    def get_download_url(self, file):
        return Blob(file).create_download_url()

    class Meta:
        model = File
        fields = (
            "download_command",
            "download_url",
        )


class FilePOSTSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        exclude = (
            "classification",
            "description",
            "nr_downloads",
            "processed",
            "published",
            "sensor",
            "study",
            "zone",
        )


class FilePUTSerializer(serializers.ModelSerializer):
    classification = serializers.PrimaryKeyRelatedField(
        queryset=Classification.objects.all(), allow_null=False, required=False
    )
    sensor = serializers.PrimaryKeyRelatedField(
        queryset=Sensor.objects.all(), allow_null=True, required=False
    )
    study = serializers.PrimaryKeyRelatedField(
        queryset=Study.objects.all(), allow_null=False, required=False
    )
    zone = serializers.PrimaryKeyRelatedField(
        queryset=Zone.objects.all(), allow_null=True, required=False
    )

    def update(self, instance, validated_data):
        processed = validated_data.get("processed", instance.processed)
        published = validated_data.get("published", instance.published)
        if not processed and published:
            raise serializers.ValidationError("An unprocessed file can't be published")
        instance.published = published
        instance.processed = processed

        instance.classification = validated_data.get(
            "classification", instance.classification
        )
        instance.sensor = validated_data.get("sensor", instance.sensor)
        instance.study = validated_data.get("study", instance.study)
        instance.zone = validated_data.get("zone", instance.zone)
        instance.description = validated_data.get("description", instance.description)
        instance.name = validated_data.get("name", instance.name)

        instance.save()
        return instance

    class Meta:
        model = File
        exclude = (
            "extension",
            "md5_hash",
            "nr_downloads",
            "size",
            "url",
        )


class StudyPOSTSerializer(serializers.ModelSerializer):
    class Meta:
        model = Study
        fields = "__all__"


class StudyGETSerializer(serializers.ModelSerializer):
    class Meta:
        model = Study
        fields = "__all__"

    chapter = RelatedFieldSerializer(Chapter)
    nr_files = serializers.IntegerField(source="file_set.count")


class ZoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Zone
        fields = "__all__"


class ClassificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Classification
        fields = "__all__"


class ExtensionSerializer(serializers.ModelSerializer):
    class Meta:
        model = FileExtension
        fields = "__all__"


class IdsListSerializer(serializers.Serializer):
    ids = serializers.ListField(child=serializers.IntegerField())
