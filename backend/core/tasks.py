import time

from audit.models import Log
from celery import shared_task
from core.azure_storage import Blob
from core.models import File, FileStatusRename
from django.shortcuts import get_object_or_404


@shared_task
def rename_blob(file_id: int, original_name: str, desired_name: str) -> None:
    """Rename blob on Azure Storage. First copies the blob and, if that succeeded,
    the original blob is deleted.
    This function is executed by a Celery task.
    """
    self_name = "rename_blob"
    Log(
        function=self_name,
        message=f"Start rename of '{original_name}' to '{desired_name}'.",
    ).save()

    # Allow calling method to finish, to avoid race conditions with
    # fetching/saving the file here
    time.sleep(1)

    # We have to fetch the file here, and instantiate the blob object, because it
    # is not allowed to use function arguments that are not JSON serializable in a
    # Celery task.
    file = get_object_or_404(File, pk=file_id)
    blob = Blob(file)
    destination_path = blob.path.replace(original_name, desired_name)
    successfull_copied = blob.copy_blob(destination_path)

    successfull_deleted = blob.delete_blob() if successfull_copied else False

    if successfull_copied and successfull_deleted:
        file.status_rename = FileStatusRename.SUCCESS.value
        file.url = blob.url.replace(original_name, desired_name)
        file.save()
        Log(
            function=self_name,
            message=f"Rename of '{original_name}' to '{desired_name}' success.",
        ).save()
        return

    file.name = original_name
    file.status_rename = FileStatusRename.FAILED.value
    file.save()
    Log(
        function=self_name,
        message=f"Rename of '{original_name}' to '{desired_name}' failed.",
    ).save()
