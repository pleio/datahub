"""
Renders custom variables to html templates.
"""

from django.conf import settings


def global_settings(request):
    """
    Returns global settings from the settings file to html templates.

    Args:
        request : object
            Object containing the HTTP request info from Flask.
    """

    return {
        "AZURE_ACCOUNT_NAME": settings.AZURE_ACCOUNT_NAME,
        "AZURE_CONTAINER_NAME": settings.AZURE_CONTAINER_NAME,
        "PROFILE_URL": settings.PROFILE_URL,
        "ENVIRONMENT": settings.ENVIRONMENT,
        "CI_COMMIT_SHA": settings.CI_COMMIT_SHA,
        "CI_COMMIT_TAG": settings.CI_COMMIT_TAG,
    }
