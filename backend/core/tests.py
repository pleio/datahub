import json
from datetime import datetime, timezone
import os
import urllib.parse
from datetime import datetime
from unittest.mock import patch

from core.models import (
    Chapter,
    Classification,
    DatahubUser,
    File,
    FileExtension,
    FileStatusRename,
    Sensor,
    Study,
    Zone,
)
from core.serializers import FilePUTSerializer
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import connection
from django.test.utils import CaptureQueriesContext
from django.urls import reverse
from rest_framework import serializers, status
from rest_framework.test import APITestCase


def create_file(
    file_name,
    sensor=None,
    zone=None,
    classification=None,
    published=True,
    processed=True,
    study=None,
):
    data = {
        "name": file_name,
        "description": "A file for testing",
        "published": published,
        "processed": processed,
        "size": 500,
        "sensor": sensor,
        "study": study,
        "zone": zone,
        "url": "www.wonderbit.com",
        "classification": classification,
    }
    return File.objects.create(**data)


def create_user(staff):
    """
    In django a regular admin is a user with:
        is_staff: True
        is_superuser: False
    """
    try:
        last_id = DatahubUser.objects.last().id
    except AttributeError:
        last_id = "0"
    return DatahubUser.objects.create(
        username="test_user",
        password="secret",
        is_superuser=False,
        is_staff=staff,
        email=f"test{last_id}@user",
    )


class HealthTests(APITestCase):
    def test_health(self):
        """Ensure we can make simple get requests"""
        url = reverse("api-v1:api-v1-health")
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CreateFileTestCase(APITestCase):
    """Tests on creation of files.
    If a file is uploaded to Azure Blob Storage, this should
    trigger the creation of a file in the database
    """

    @patch("core.views.CreateFile._authenticated_request")
    def test_valid_handshake(self, mock_authenicated_request):
        """
        For synchronous handshake the validationcode should be returned
        https://learn.microsoft.com/en-us/azure/event-grid/webhook-event-delivery
        """
        response_data_handshake = [
            {
                "id": "58dd81f9",
                "topic": "/subscriptions/757575/wonderbitteststorage1",
                "subject": "",
                "data": {"validationCode": "12345", "validationUrl": "a_long_url"},
                "eventType": "Microsoft.EventGrid.SubscriptionValidationEvent",
                "eventTime": "2022-11-23T14:43:31.8118342Z",
                "metadataVersion": "1",
                "dataVersion": "2",
            }
        ]

        mock_authenicated_request.return_value = True
        response = self.client.post(
            reverse("createfile"),
            data=json.dumps(response_data_handshake),
            content_type="application/json",
        )
        self.assertEqual(response.data["validationResponse"], "12345")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_request(self):
        """The request doesn't satisfy certain assumptions"""
        response = self.client.post(
            reverse("createfile"),
            data=json.dumps({"foo": "bar"}),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @patch.dict(
        os.environ, {"AZURE_SUBSCRIPTION_ID": "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaa"}
    )
    def test_unauthenticated_request(self):
        """The request doesn't have the correct subscription id"""
        dummy_subscription_id = "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
        payload = [
            {
                "topic": f"/subscriptions/{dummy_subscription_id}/foo",
                "data": {"dummy_value": "dummy_value"},
            }
        ]

        response = self.client.post(
            reverse("createfile"),
            data=json.dumps(payload),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue("not authenticated" in response.data["message"])

    @patch("core.views.CreateFile._authenticated_request")
    @patch("core.views.get_md5_hash")
    def test_create_file(self, mock_get_md5_hash, mock_authenicated_request):
        """Correct input should trigger creation of file"""
        response_data_upload = [
            {
                "id": "7f7e05c9",
                "topic": "/subscriptions/757575/wonderbitteststorage1",
                "subject": "/blobServices/default/containers/container/2022/11/file.txt",
                "data": {
                    "api": "PutBlob",
                    "clientRequestId": "c21bc50a-ef0b-4c17-5b98-3b88f085065c",
                    "requestId": "7f7e05c9-d01e-001e-4e4a-ffec1c000000",
                    "eTag": "0x8DACD61BD696C60",
                    "contentType": "image/jpeg",
                    "contentLength": 3334,
                    "blobType": "BlockBlob",
                    "url": "https://wonderbit/windows.net/foo/file.txt",
                    "sequencer": "af2828",
                    "storageDiagnostics": {"batchId": "ef13b3b6"},
                },
                "eventType": "Microsoft.Storage.BlobCreated",
                "eventTime": "2022-11-23T14:48:09.9681127Z",
                "metadataVersion": "1",
                "dataVersion": "",
            }
        ]

        mock_authenicated_request.return_value = True
        mock_get_md5_hash.return_value = "abc123"
        current_extensions = FileExtension.objects.all()
        self.assertFalse(current_extensions.exists())
        response = self.client.post(
            reverse("createfile"),
            data=json.dumps(response_data_upload),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        current_extensions = set(f.name for f in FileExtension.objects.all())
        self.assertEqual(current_extensions, set(["TXT"]))


class OnlyUnprocessedAlsoUnpublishedTest(APITestCase):
    """GET requests for files can be filtered by two optional boolean parameters;
    'only_unprocessed' and 'also_unpublished'. This gives four possible combinations for
    requests. The files have two fields; 'published' and 'processed' that correspond
    with this filters.

    The confusing bit is that 'only_unprocessed' is a filter itself, so
    'only_unprocessed=false' means 'files with processed=false', and
    'only_unprocessed=true' means 'files with processed=true'.
    On the other hand is 'also_unpublished' a flag for applying a filter, so
    'also_unpublished=true' means both 'published=false' and 'published=true', and
    'also_unpublished=false' means only 'published=true'.

    The behaviour also depends on the users permissions, only admin users are allowed
    to use the parameters above.

    In this class the possible combinations are systematically and exhaustive tested.
    """

    @classmethod
    def setUpTestData(cls):
        # Create a testset of files with all four possible combinations of
        # published/processed
        cls.file_published_processed = create_file(
            "file_published_processed", published=True, processed=True
        )
        cls.file_unpublished_processed = create_file(
            "file_unpublished_processed", published=False, processed=True
        )
        cls.file_unpublished_unprocessed = create_file(
            "file_unpublished_unprocessed", published=False, processed=False
        )
        # This last combination should not be possible. A published file should always
        # be processed. Therefore this gives a validation error, but it is included here
        # for the sake of completeness.
        with cls.assertRaises(cls, ValidationError):
            cls.file_published_unprocessed = create_file(
                "file_published_unprocessed", published=True, processed=False
            )
        cls.admin = create_user(staff=True)
        cls.regular_user = create_user(staff=False)

    def test_also_unpublished_true_only_unprocessed_true(self):
        # Arrange
        url = (
            reverse("api-v1:api-v1-files")
            + "?also_unpublished=true&only_unprocessed=true"
        )

        # --- Anonymous user ---
        response = self.client.get(url, format="json")
        self.assertEqual(response.data["count"], 1)

        # --- Regular user ---
        self.client.force_authenticate(user=self.regular_user)
        response = self.client.get(url, format="json")
        self.assertEqual(response.data["count"], 1)

        # --- Admin user ---
        self.client.force_authenticate(user=self.admin)
        response = self.client.get(url, format="json")
        self.assertEqual(response.data["count"], 1)

    def test_also_unpublished_false_only_unprocessed_true(self):
        # This is a conflicting request because 'only_unprocessed=true' means only
        # files with 'processed=false', and 'processed=false' implies 'published=false'.
        # But 'also_unpublished=false' means only files with 'published=true'.
        # This doesn't make sense, so the implementation short circuits from
        # 'only_unprocessed=true'. The 'also_unpublished' parameter is ignored and
        # 'also_unpublished=true' is assumed.

        # Arrange
        url = (
            reverse("api-v1:api-v1-files")
            + "?also_unpublished=false&only_unprocessed=true"
        )

        # --- Anonymous user ---
        response = self.client.get(url, format="json")
        self.assertEqual(response.data["count"], 1)

        # --- Regular user ---
        self.client.force_authenticate(user=self.regular_user)
        response = self.client.get(url, format="json")
        self.assertEqual(response.data["count"], 1)

        # --- Admin user ---
        self.client.force_authenticate(user=self.admin)
        response = self.client.get(url, format="json")
        self.assertEqual(response.data["count"], 1)

    def test_also_unpublished_true_only_unprocessed_false(self):
        # Arrange
        url = (
            reverse("api-v1:api-v1-files")
            + "?also_unpublished=true&only_unprocessed=false"
        )

        # --- Anonymous user ---
        response = self.client.get(url, format="json")
        self.assertEqual(response.data["count"], 1)

        # --- Regular user ---
        self.client.force_authenticate(user=self.regular_user)
        response = self.client.get(url, format="json")
        self.assertEqual(response.data["count"], 1)

        # --- Admin user ---
        self.client.force_authenticate(user=self.admin)
        response = self.client.get(url, format="json")
        self.assertEqual(response.data["count"], 2)

    def test_also_unpublished_false_only_unprocessed_false(self):
        # Arrange
        url = (
            reverse("api-v1:api-v1-files")
            + "?also_unpublished=false&only_unprocessed=false"
        )

        # --- Anonymous user ---
        response = self.client.get(url, format="json")
        self.assertEqual(response.data["count"], 1)

        # --- Regular user ---
        self.client.force_authenticate(user=self.regular_user)
        response = self.client.get(url, format="json")
        self.assertEqual(response.data["count"], 1)

        # --- Admin user ---
        self.client.force_authenticate(user=self.admin)
        response = self.client.get(url, format="json")
        self.assertEqual(response.data["count"], 1)


class PostTests(APITestCase):
    """
    Tests for the C in CRUD
    """

    @classmethod
    def setUpTestData(cls):
        cls.admin = create_user(staff=True)

    def test_create_study_succes(self):
        """Succesfully create a study"""

        data = {"name": "Valid data"}

        self.client.force_authenticate(user=self.admin)
        response = self.client.post(
            reverse("api-v1:api-v1-studies"),
            data=json.dumps(data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class TotalsGetTests(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.admin = create_user(staff=True)

    def test_get_totals_unauthorized(self):
        """Totals should not be visible it not authorized (user not admin)"""
        # Arrange
        url = reverse("api-v1:api-v1-totals")

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_totals_authorized(self):
        """Totals should be visible it authorized (admin)"""
        # Arrange
        url = reverse("api-v1:api-v1-totals")
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class FilesGetTests(APITestCase):
    """Get list of files"""

    @classmethod
    def setUpTestData(cls):
        cls.admin = create_user(staff=True)
        cls.regular_user = create_user(staff=False)

    def test_get_files(self):
        """Ensure we can get the files,
        and getting more files doesn't require more queries"""
        # Arrange
        url = reverse("api-v1:api-v1-files")
        for _ in range(5):
            create_file("file")

        # Get the nr of queries needed to fetch all files from the database
        with CaptureQueriesContext(connection) as queries_before:
            self.client.get(url, format="json")

        # Increase the nr of files in the database
        for _ in range(5):
            create_file("file")

        # Act
        # Get the nr of queries needed to fetch more files from the database than before
        with CaptureQueriesContext(connection) as queries_after:
            response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertLessEqual(len(queries_after), len(queries_before))

    def test_file_search(self):
        """Ensure we can search files on name"""
        # Arrange
        create_file("file_1")
        create_file("file_2")
        url = reverse("api-v1:api-v1-files") + "?search=file_1"

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)

    def test_file_ordering(self):
        """Ensure we can sort the files"""

        # Arrange (global)
        for file_name in ("file-a", "file-b", "file-A"):
            create_file(file_name)

        # --- No specific ordering ---(Note default ordering on modification date)
        # Arrange
        url = reverse("api-v1:api-v1-files")

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertNotEqual(response.data["results"][0]["name"], "file-b")

        # --- Ordering on name ---
        # Arrange
        url = reverse("api-v1:api-v1-files") + "?ordering=name"

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.data["results"][0]["name"], "file-a")

        # The ordering is case insensitive, meaning we get file-a, file-A, file-b
        # and NOT file-a, file-b, file-A
        self.assertEqual(response.data["results"][1]["name"], "file-A")

    def test_file_filter_on_sensor(self):
        """Ensure we can filter files, e.g. on sensor"""
        # Arrange
        sensor = Sensor.objects.create(name="sensor")
        create_file("file_with_sensor", sensor=sensor)
        create_file("file_without_sensor")
        url = reverse("api-v1:api-v1-files") + f"?sensor={sensor.id}"

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)

    def test_file_filter_multiple_classifications(self):
        """Ensure files can be filtered on multiple classifications at once"""
        # Arrange
        classif_1 = Classification.objects.create(name="classif_1")
        classif_2 = Classification.objects.create(name="classif_2")
        classif_3 = Classification.objects.create(name="classif_3")
        for i in range(4):
            if i % 2:
                create_file(f"file_{i}", classification=classif_1)
            else:
                create_file(f"file_{i}", classification=classif_2)
        create_file(f"file_{i}", classification=classif_3)

        url = (
            reverse("api-v1:api-v1-files")
            + f"?classification={classif_1.id}&classification={classif_2.id}"
        )

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 4)

    def test_file_filter_multiple_zones(self):
        """Ensure files can be filtered on multiple zones at once"""
        # Arrange
        zone_1 = Zone.objects.create(name="zone_1", published=True)
        zone_2 = Zone.objects.create(name="zone_2", published=False)
        zone_3 = Zone.objects.create(name="zone_3", published=True)
        zone_4 = Zone.objects.create(name="zone_4", published=True)
        zone_5 = Zone.objects.create(name="zone_5", published=True)

        create_file("file_1", zone=zone_1)
        create_file("file_2", zone=zone_2)
        create_file("file_3", zone=zone_3)
        create_file("file_4", zone=zone_4)
        create_file("file_5", zone=zone_5)

        url = (
            reverse("api-v1:api-v1-files")
            + f"?zone={zone_1.id}&zone={zone_2.id}&zone={zone_3.id}&zone={zone_4.id}"
        )

        # Act
        response = self.client.get(url, format="json")

        # Assert
        expected_in_result = (
            "file_1",
            # "file_2",  Excluded because the zone is unpublished
            "file_3",
            "file_4",
            # "file_5",  Excluded because zone_5 not requested
        )
        for result in response.data["results"]:
            self.assertTrue(result["name"] in expected_in_result)
        self.assertTrue(len(response.data["results"]), len(expected_in_result))

    def test_file_filter_multiple_zones_and_classifications(self):
        """Ensure files can be filtered on multiple zones, and multiple classifications
        at once
        """
        # Assert
        zone_1 = Zone.objects.create(name="zone_1", published=True)
        zone_2 = Zone.objects.create(name="zone_2", published=False)
        zone_3 = Zone.objects.create(name="zone_3", published=True)
        zone_4 = Zone.objects.create(name="zone_4", published=True)
        classif_1 = Classification.objects.create(name="classif_1")
        classif_2 = Classification.objects.create(name="classif_2")
        classif_3 = Classification.objects.create(name="classif_3")

        create_file("file_1", zone=zone_1, classification=classif_1)
        create_file("file_2", zone=zone_2)
        create_file("file_3", zone=zone_3, classification=classif_1)
        create_file("file_4", zone=zone_4, classification=classif_2)
        create_file("file_5", zone=zone_3, classification=classif_3)

        url = (
            reverse("api-v1:api-v1-files") + f"?zone={zone_1.id}&zone={zone_2.id}"
            f"&zone={zone_3.id}&classification={classif_1.id}&{classif_2.id}"
        )

        # Act
        response = self.client.get(url, format="json")

        # Assert
        expected_in_result = (
            "file_1",
            # "file_2",  Excluded because the zone is unpublished
            "file_3",
            # "file_4",  Excluded because zone_4 not requested
            # "file_5",  Excluded because classification 3 not requested
        )
        for result in response.data["results"]:
            self.assertTrue(result["name"] in expected_in_result)
        self.assertTrue(len(response.data["results"]), len(expected_in_result))

    def test_file_filter_unused_classification(self):
        """Ensure files can be filtered on unused classifications"""
        # Arrange
        create_file("file_1")
        create_file("file_2")
        url = reverse("api-v1:api-v1-files") + "?classification=-3"

        # Act
        response = self.client.get(url, format="json").data

        # Assert
        self.assertEqual(len(response["results"]), 0)

    def test_file_filter_classification_combined(self):
        """Files can be filtered on combinations of used and unused classifications"""
        # Arrange
        classif = Classification.objects.create(name="classif")
        create_file("file_1")
        create_file("file_2", classification=classif)
        url = (
            reverse("api-v1:api-v1-files")
            + f"?classification={classif.id}&classification=-4"
        )

        # Act
        response = self.client.get(url, format="json").data

        # Assert
        self.assertEqual(len(response["results"]), 1)

    def test_hide_unprocessed_files_detail(self):
        """Unprocessed files are not returned in the files detail"""
        # Arrange
        file = create_file("unprocessed_file")
        file.published = False
        file.processed = False
        file.save()
        self.client.force_authenticate(user=self.admin)
        url = (
            reverse("api-v1:api-v1-file", kwargs={"pk": file.id})
            + "?also_unpublished=true"
        )

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertFalse(response.data.get("id"))

    def test_filter_files_on_modified_after(self):
        """Files can be filtered on modification datetime"""
        # Arrange
        now = datetime.now(tz=timezone.utc)
        create_file(f"created after {now}")

        now = urllib.parse.quote(str(now))
        url = reverse("api-v1:api-v1-files") + f"?modified_after={now}"
        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(len(response.data["results"]), 1)

    def test_filter_files_on_modified_before(self):
        """Files can be filtered on modification datetime"""
        # Arrange
        create_file("file_1")
        create_file("file_2")
        nr_objects_before = File.objects.count()
        now = datetime.now(tz=timezone.utc)
        create_file(f"created after {now}")
        now = urllib.parse.quote(str(now))
        url = reverse("api-v1:api-v1-files") + f"?modified_before={now}"

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(len(response.data["results"]), nr_objects_before)

    def test_filter_files_on_modification_date_invalid_input(self):
        """An invalid filter request returns a 400"""
        # Arrange
        url = reverse("api-v1:api-v1-files") + "?modified_before=%.3:+"

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_hide_unprocessed_files(self):
        """Unprocessed files are not returned in the files list
        Note that unprocessed implies unpublished
        """
        # Arrange
        create_file("processed_file", published=False)
        file = create_file("unprocessed_file", published=False)
        file.processed = False
        file.save()
        url = reverse("api-v1:api-v1-files") + "?also_unpublished=true"
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertNotEqual(response.data["count"], 2)
        self.assertTrue(response.data["results"][0]["processed"])

    def test_show_unprocessed_files_if_requested(self):
        """Unprocessed files are returned if admin and explicitly requested"""
        # Arrange
        create_file("processed_file", published=False)
        file = create_file("unprocessed_file", published=False)
        file.processed = False
        file.save()

        url = reverse("api-v1:api-v1-files") + "?only_unprocessed=true"
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.data["count"], 1)
        self.assertFalse(response.data["results"][0]["processed"])

    def test_hide_file_if_related_study_unpublished(self):
        """A file is not returned if the related study is unpublished"""
        # Arrange
        study = Study.objects.create(name="study_1")
        create_file("file_1", study=study)
        url = reverse("api-v1:api-v1-files")

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.data["count"], 0)

    def test_hide_file_if_related_zone_unpublished(self):
        """A file is not returned if the related zone is unpublished"""
        # Arrange
        zone = Zone.objects.create(name="zone_1")
        create_file("file_1", zone=zone)
        url = reverse("api-v1:api-v1-files")

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.data["count"], 0)


class StudyGetTests(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.admin = create_user(staff=True)
        cls.regular_user = create_user(staff=False)

    def test_study_pagination(self):
        """Pagination of studies"""
        # Arrange
        for i in range(settings.MAX_PAGINATION + 5):
            Study.objects.create(name=f"study_{i}", published=True)

        # --- Default pagination ---
        url = reverse("api-v1:api-v1-studies")
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), settings.DEFAULT_PAGINATION)

        # --- Limited pagination ---
        # Arrange
        url = reverse("api-v1:api-v1-studies") + f"?limit={settings.MIN_PAGINATION}"

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(len(response.data["results"]), settings.MIN_PAGINATION)

        # --- Authorized to turn pagination off ---
        # Arrange
        self.client.force_authenticate(user=self.admin)
        url = reverse("api-v1:api-v1-studies") + "?pagination=false"

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(len(response.data["results"]), Study.objects.count())

        # --- Not authorized to turn pagination off ---
        # Arrange
        self.client.force_authenticate(user=self.regular_user)

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(len(response.data["results"]), settings.DEFAULT_PAGINATION)

    def test_study_search(self):
        """Studies can be searched by chapter"""
        # Arrange
        chapter_1 = Chapter.objects.create(name="chapter_1")
        Study.objects.create(name="study_1", published=True)
        Study.objects.create(name="study_2", published=True, chapter=chapter_1)
        Study.objects.create(name="study_3", published=True)
        url = reverse("api-v1:api-v1-studies") + "?search=chapter_1"

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["name"], "study_2")

    def test_study_ordering(self):
        """Studies can be ordered, eg. on chapter"""
        # Arrange
        chapter_1 = Chapter.objects.create(name="chapter_1")
        chapter_2 = Chapter.objects.create(name="chapter_2")
        Study.objects.create(name="study_1", published=True)
        Study.objects.create(name="study_2", published=True, chapter=chapter_2)
        Study.objects.create(name="study_3", published=True, chapter=chapter_1)
        Study.objects.create(name="study_4", published=True)
        Study.objects.create(name="study_5", published=True, chapter=chapter_2)
        url = reverse("api-v1:api-v1-studies") + "?ordering=chapter__name"

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), Study.objects.count())
        expected_names = (
            "study_3",  # chapter_1
            "study_2",  # chapter_2
            "study_5",  # chapter_2
            "study_1",
            "study_4",
        )
        for study, name in zip(response.data["results"], expected_names):
            self.assertEqual(study["name"], name)

    def test_studies_filter(self):
        """Studies can be filtered, e.g. on chapter"""
        # Arrange
        chapter_1 = Chapter.objects.create(name="chapter_1")
        Study.objects.create(name="study_1", published=True)
        Study.objects.create(name="study_2", published=True, chapter=chapter_1)
        Study.objects.create(name="study_3", published=True)
        Study.objects.create(name="study_4", published=True, chapter=chapter_1)
        url = reverse("api-v1:api-v1-studies") + f"?chapter={chapter_1.id}"

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 2)
        study_names_response = set(study["name"] for study in response.data["results"])
        self.assertEqual(study_names_response, set(("study_2", "study_4")))


class ChapterGetTests(APITestCase):
    def test_chapter_search(self):
        """Chapters can be searched, e.g. on name"""
        # Arrange
        Chapter.objects.create(name="chapter_1")
        url = reverse("api-v1:api-v1-chapters") + "?search=chapter_1"

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["name"], "chapter_1")

    def test_chapter_ordering(self):
        """Chapters can be ordered, e.g. on name"""
        # Arrange
        Chapter.objects.create(name="chapter_2")
        Chapter.objects.create(name="chapter_1")
        Chapter.objects.create(name="chapter_3")
        url = reverse("api-v1:api-v1-chapters") + "?ordering=name"

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 3)
        self.assertEqual(response.data["results"][0]["name"], "chapter_1")


class ZoneGetTests(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.admin = create_user(staff=True)

    def test_zone_search(self):
        """Zones can be searched, e.g. on name"""
        # Arrange
        Zone.objects.create(name="zone_1", published=True)
        url = reverse("api-v1:api-v1-zones") + "?search=zone_1"

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["name"], "zone_1")

    def test_zone_ordering(self):
        """Zones can be ordered, e.g. on published"""
        # Arrange
        Zone.objects.create(name="zone_1", published=True)
        Zone.objects.create(name="zone_2")
        Zone.objects.create(name="zone_3", published=True)
        Zone.objects.create(name="zone_4")
        url = (
            reverse("api-v1:api-v1-zones") + "?ordering=published&also_unpublished=True"
        )
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.get(url, format="json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), Zone.objects.count())
        self.assertEqual(response.data["results"][0]["name"], "zone_2")
        self.assertEqual(response.data["results"][1]["name"], "zone_4")
        self.assertEqual(response.data["results"][2]["name"], "zone_1")
        self.assertEqual(response.data["results"][3]["name"], "zone_3")

        expected_names = (
            "zone_2",  # unpublished
            "zone_4",  # unpublished
            "zone_1",  # published
            "zone_3",  # published
        )
        for zone, name in zip(response.data["results"], expected_names):
            self.assertEqual(zone["name"], name)


class FileDownloadGetTests(APITestCase):
    """Get download info for single file"""

    def test_get_download_link(self):
        """Test getting a file download link"""
        # Arrange
        file_1 = create_file("file_1")
        date_modified_before = file_1.date_modified
        url = reverse("api-v1:api-v1-file-download", kwargs={"pk": file_1.id})

        # Act
        response = self.client.get(url, content_type="application/json")
        file_1.refresh_from_db()

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            list(response.data.keys()),
            ["id", "download_command", "download_url"],
        )
        self.assertEqual(file_1.date_modified, date_modified_before)

    def test_get_download_link_renaming_in_progress(self):
        """Test getting a file download link of a file with renaming in progress"""
        # Arrange
        file_1 = create_file("file_1")
        file_1.status_rename = FileStatusRename.IN_PROGRESS.value
        file_1.save()
        date_modified_before = file_1.date_modified
        url = reverse("api-v1:api-v1-file-download", kwargs={"pk": file_1.id})

        # Act
        response = self.client.get(url, content_type="application/json")
        file_1.refresh_from_db()

        # Assert
        self.assertEqual(response.status_code, status.HTTP_423_LOCKED)
        self.assertEqual(file_1.date_modified, date_modified_before)


class FilesDownloadGetTests(APITestCase):
    """Get download info for multiple files"""

    @classmethod
    def setUpTestData(cls):
        cls.admin = create_user(staff=True)

    def test_filesdownload(self):
        """
        Test file download links for multiple files
        http://localhost:8000/api/v1/files/downloads?ids=1,2
        """
        # Arrange
        file_1 = create_file("file_1")
        file_2 = create_file("file_2")
        url = reverse("api-v1:api-v1-files-download") + f"?ids={file_1.id},{file_2.id}"

        # Act
        response = self.client.get(url, content_type="application/json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(
            list(response.data[0].keys()),
            ["id", "download_command", "download_url"],
        )

    def test_filesdownload_invalid_request_id(self):
        """Test file download links for multiple files, with an invalid request.
        http://localhost:8000/api/v1/files/downloads?ids=abcd
        """
        # Arrange
        file_1 = create_file("file_1")
        url = reverse("api-v1:api-v1-files-download") + f"?ids={file_1.id},abc"

        # Act
        response = self.client.get(url, content_type="application/json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_filesdownload_invalid_request_None(self):
        """Test file download links for multiple files, with an invalid request.
        http://localhost:8000/api/v1/files/downloads?ids=
        """
        # Arrange
        file_1 = create_file("file_1")
        url = reverse("api-v1:api-v1-files-download") + f"?ids={file_1.id},{None}"

        # Act
        response = self.client.get(url, content_type="application/json")

        # Assert
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_filesdownload_renaming_in_progress(self):
        """Test getting a file download links of a file with renaming in progress"""
        # Arrange
        file_1 = create_file("file_1")
        file_1.status_rename = FileStatusRename.IN_PROGRESS.value
        file_1.save()
        file_2 = create_file("file_2")
        date_modified_before = file_1.date_modified
        url = reverse("api-v1:api-v1-files-download") + f"?ids={file_1.id},{file_2.id}"

        # Act
        response = self.client.get(url, content_type="application/json")
        file_1.refresh_from_db()

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(file_1.date_modified, date_modified_before)
        self.assertEqual(len(response.data), 1)
        # Excluded cause renaming in progress
        self.assertFalse(file_1.id == response.data[0]["id"])


class ZoneUpdateTests(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.admin = create_user(staff=True)

    def test_valid_unpublished_zone_update(self):
        """Succesfully update a zone, published False -> True"""
        # Arrange
        zone = Zone.objects.create(name="zone_A", published=False)

        payload = {"name": "Valid data", "published": True}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-zone", kwargs={"pk": zone.id}),
            data=json.dumps(payload),
            content_type="application/json",
        )
        zone.refresh_from_db()

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(zone.published)

    def test_valid_published_zone_update(self):
        """Succesfully update a zone, published True -> False"""
        # Arrange
        zone = Zone.objects.create(name="zone_B", published=True)
        payload = {"name": "Valid data", "published": False}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-zone", kwargs={"pk": zone.id}),
            data=json.dumps(payload),
            content_type="application/json",
        )
        zone.refresh_from_db()

        # Assert
        self.assertFalse(zone.published)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class StudyUpdateTests(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.admin = create_user(staff=True)

    def test_valid_unpublished_study_update(self):
        """
        Succesfully update a study, published False -> True
        """
        # Arrange
        study = Study.objects.create(name="study_Bar", chapter=None, published=False)
        payload = {"name": "Valid data", "published": True}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-study", kwargs={"pk": study.id}),
            data=json.dumps(payload),
            content_type="application/json",
        )
        study.refresh_from_db()

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(study.published)

    def test_valid_published_study_update(self):
        """
        Succesfully update a study, published True -> False
        """
        # Arrange
        study = Study.objects.create(name="study_Bar", chapter=None, published=True)
        payload = {"name": "Valid data", "published": False}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-study", kwargs={"pk": study.id}),
            data=json.dumps(payload),
            content_type="application/json",
        )
        study.refresh_from_db()

        # Assert
        self.assertFalse(study.published)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class FileUpdateTests(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.admin = create_user(staff=True)

    def test_file_update_classification(self):
        """
        Succesfully update a file, taking previous values for fields that are not
        provided
        """
        # Arrange
        classif_old = Classification.objects.create(name="classif_old")
        study_old = Study.objects.create(name="study_old")
        classif_new = Classification.objects.create(name="classif_new")
        file = create_file("testfile", classification=classif_old, study=study_old)
        payload = {"classification": classif_new.id}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-file", kwargs={"pk": file.id}),
            data=json.dumps(payload),
            content_type="application/json",
        )
        file.refresh_from_db()

        # Assert
        self.assertEqual(file.classification.name, "classif_new")
        self.assertEqual(file.study.name, "study_old")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_file_update_sensor_to_none(self):
        """Update the sensor of a file to None"""
        # Arrange
        sensor_old = Sensor.objects.create(name="sensor_old")
        payload = {"sensor": None}
        file = create_file("testfile", sensor=sensor_old)
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-file", kwargs={"pk": file.id}),
            data=json.dumps(payload),
            content_type="application/json",
        )
        file.refresh_from_db()

        # Assert
        self.assertFalse(file.sensor)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_valid_file_update_zone(self):
        """Update zone on a file"""
        # Arrange
        zone = Zone.objects.create(name="zone_1", published=True)
        file = create_file("testfile")

        payload = {"zone": zone.id}
        file = File.objects.last()
        self.client.force_authenticate(user=self.admin)

        # Assume
        self.assertFalse(file.zone)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-file", kwargs={"pk": file.id}),
            data=json.dumps(payload),
            content_type="application/json",
        )
        file.refresh_from_db()

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(file.zone.name, zone.name)

    def test_valid_file_update_zone_to_none(self):
        """Update zone on a file to None"""
        # Arrange
        zone = Zone.objects.create(name="zone_1")
        file = create_file("testfile", zone=zone)
        payload = {"zone": None}
        self.client.force_authenticate(user=self.admin)

        # Assume
        self.assertTrue(file.zone)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-file", kwargs={"pk": file.id}),
            data=json.dumps(payload),
            content_type="application/json",
        )
        file.refresh_from_db()

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(file.zone)

    def test_file_update_size(self):
        """Some fields, like size, can't be updated, so these are ignored"""
        # Arrange
        file = create_file("testfile")
        file_size_before = file.size
        zone = Zone.objects.create(name="zone_1")
        payload = {"zone": zone.id, "size": 1_234}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-file", kwargs={"pk": file.id}),
            data=json.dumps(payload),
            content_type="application/json",
        )
        file.refresh_from_db()

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(file.size, file_size_before)  # size is not updated
        self.assertEqual(file.zone.name, zone.name)  # other fields do get updated

    def test_invalid_file_update_classification(self):
        """
        No update of a file if data is invalid.
        Here: Classification can't be None
        """
        # Arrange
        payload = {"name": "filename", "classification": None}
        file = create_file("testfile")
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-file", kwargs={"pk": file.id}),
            data=json.dumps(payload),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue(
            "This field may not be null" in response.data["classification"][0]
        )

    def test_invalid_file_update_published_on_unprocessed(self):
        """
        No update of a file if data is invalid.
        Here: Putting an unprocessed file to published
        """
        # Arrange
        classif = Classification.objects.create(name="classification")
        sensor = Sensor.objects.create(name="sensor")
        zone = Zone.objects.create(name="zone")
        study = Study.objects.create(name="study")
        extension = FileExtension.objects.create(name="extension")
        file = create_file("testfile")
        file.published = False
        file.processed = False
        file.save()
        payload = {
            "classification": classif.id,
            "sensor": sensor.id,
            "zone": zone.id,
            "study": study.id,
            "extension": extension.id,
            "published": True,
        }
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-file", kwargs={"pk": file.id}),
            data=json.dumps(payload),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue("An unprocessed file can't be published" in response.data[0])

    def test_valid_file_update_published_on_processed(self):
        """
        Update a file if data is valid.
        Here: Putting a processed file to published
        """
        # Arrange
        classif = Classification.objects.create(name="classification")
        sensor = Sensor.objects.create(name="sensor")
        zone = Zone.objects.create(name="zone")
        study = Study.objects.create(name="study")
        extension = FileExtension.objects.create(name="extension")
        file = create_file("testfile")
        file.published = False
        file.save()
        payload = {
            "classification": classif.id,
            "sensor": sensor.id,
            "zone": zone.id,
            "study": study.id,
            "extension": extension.id,
            "published": True,
        }
        self.client.force_authenticate(user=self.admin)

        # Assume
        self.assertTrue(file.processed)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-file", kwargs={"pk": file.id}),
            data=json.dumps(payload),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @patch("core.tasks.rename_blob.delay")
    def test_rename_file(self, mock_rename_blob_delay):
        """A file can be renamed"""
        # Arrange
        mock_rename_blob_delay.return_value = True
        file_1 = create_file("file_1")
        payload = {"name": "file_1_renamed"}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-file", kwargs={"pk": file_1.id}),
            data=json.dumps(payload),
            content_type="application/json",
        )
        file_1.refresh_from_db()

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(file_1.name, payload["name"])
        self.assertEqual(file_1.status_rename, FileStatusRename.IN_PROGRESS.value)

    def test_edit_file_thats_in_progress(self):
        """A file can NOT be updated if renaming in progress"""
        # Arrange
        file_1 = create_file("file_1")
        file_1.status_rename = FileStatusRename.IN_PROGRESS.value
        file_1.save()
        payload = {"name": "file_1_renamed?", "description": "already in progress"}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-file", kwargs={"pk": file_1.id}),
            data=json.dumps(payload),
            content_type="application/json",
        )
        file_1.refresh_from_db()

        # Assert
        self.assertEqual(response.status_code, status.HTTP_423_LOCKED)
        self.assertEqual(file_1.status_rename, FileStatusRename.IN_PROGRESS.value)
        self.assertNotEqual(file_1.description, payload["description"])

    def test_rename_file_to_invalid_name(self):
        """A file can NOT be renamed to an empty filename"""
        # Arrange
        payload = {"name": "  "}

        # Act
        serializer = FilePUTSerializer(data=payload)
        with self.assertRaises(serializers.ValidationError) as context:
            serializer.is_valid(raise_exception=True)

        # Assert
        self.assertIn("This field may not be blank.", str(context.exception))


class FilesUpdateTests(APITestCase):
    """Update multiple files (unpublish)"""

    @classmethod
    def setUpTestData(cls):
        cls.admin = create_user(staff=True)

    def test_unpublish_multiple_files(self):
        """Multiple files can be unpublished at once"""
        # Arrange
        file_1 = create_file("file_1")
        file_2 = create_file("file_2")
        payload = {"ids": [file_1.id, file_2.id]}
        self.client.force_authenticate(user=self.admin)

        # Assume
        self.assertTrue(file_1.published)
        self.assertTrue(file_2.published)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-files"),
            data=json.dumps(payload),
            content_type="application/json",
        )
        file_1.refresh_from_db()
        file_2.refresh_from_db()

        # Assert
        self.assertFalse(file_1.published)
        self.assertFalse(file_2.published)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_unpublish_multiple_files_invalid_id_type(self):
        """Unpublishing multiple files with an invalid id in the
        request is handled properly
        """
        # Arrange
        payload = {"ids": ["no", "ids", "but", "strings"]}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-files"),
            data=json.dumps(payload),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_unpublish_multiple_files_invalid_ids_value(self):
        """
        Unpublishing multiple files with an invalid ids value
        is handled properly
        """
        # Arrange
        payload = {"ids": "expecting a list?"}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-files"),
            data=json.dumps(payload),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_unpublish_multiple_files_non_existing_id(self):
        """Unpublishing multiple files including non-existing id"""
        # Arrange
        file_1 = create_file("file_1")
        file_2 = create_file("file_2", published=False)
        payload = {"ids": [-4, file_1.id, file_2.id, 12_000]}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-files"),
            data=json.dumps(payload),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(response.data[204], 1)  # Only file_1
        self.assertEqual(response.data[404], 3)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_unpublish_multiple_files_date_modified(self):
        """The date_modified field is updated when unpublishing multiple files"""
        # Arrange
        file_1 = create_file("file_1")
        date_modified_before = file_1.date_modified

        data = {"ids": [file_1.id]}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.put(
            reverse("api-v1:api-v1-files"),
            data=json.dumps(data),
            content_type="application/json",
        )
        file_1.refresh_from_db()

        # Assert
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(file_1.published)
        self.assertNotEqual(date_modified_before, file_1.date_modified)


class FileDeleteTests(APITestCase):
    """Delete single file"""

    @classmethod
    def setUpTestData(cls):
        cls.admin = create_user(staff=True)

    @patch("core.azure_storage.Blob.delete_blob")
    def test_delete_file_with_study(self, mock_delete_blob):
        """
        If a file is deleted, any related objects are preserved
        like studies
        """
        # Arrange
        mock_delete_blob.return_value = True
        study = Study.objects.create(name="test-study")
        create_file("testfile", study=study)
        nr_files_before = File.objects.count()
        nr_studies_before = Study.objects.count()
        file = File.objects.last()
        self.client.force_authenticate(user=self.admin)

        # Assume
        self.assertTrue(file.study)

        # Act
        response = self.client.delete(
            reverse("api-v1:api-v1-file", kwargs={"pk": file.id}),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(File.objects.count(), nr_files_before - 1)
        self.assertEqual(Study.objects.count(), nr_studies_before)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class FilesDeleteTests(APITestCase):
    """Delete multiple files"""

    @classmethod
    def setUpTestData(cls):
        cls.admin = create_user(staff=True)

    @patch("core.azure_storage.Blob.delete_blob")
    def test_delete_multiple_files(self, mock_delete_blob):
        """Delete multiple files at once"""
        # Arrange
        mock_delete_blob.return_value = True
        for i in range(4):
            create_file(f"file_{i}")
        nr_files_before = File.objects.count()
        payload = {"ids": [File.objects.first().id, File.objects.last().id]}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.delete(
            reverse("api-v1:api-v1-files"),
            data=json.dumps(payload),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(File.objects.count(), nr_files_before - len(payload["ids"]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_multiple_files_invalid_None(self):
        """
        Delete multiple files, with invalid input: None
        """
        # Arrange
        create_file("testfile")
        nr_files_before = File.objects.count()
        data = {"ids": None}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.delete(
            reverse("api-v1:api-v1-files"),
            data=json.dumps(data),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(File.objects.count(), nr_files_before)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_multiple_files_request_string(self):
        """
        Delete multiple files, with invalid input: a string
        """
        # Arrange
        create_file("testfile")
        nr_files_before = File.objects.count()
        payload = {"ids": "a string"}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.delete(
            reverse("api-v1:api-v1-files"),
            data=json.dumps(payload),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(File.objects.count(), nr_files_before)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_multiple_files_non_existing_id(self):
        """
        Delete multiple files, with invalid input: non-existing id
        """
        # Arrange
        create_file("testfile")
        nr_files_before = File.objects.count()
        data = {"ids": [-4]}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.delete(
            reverse("api-v1:api-v1-files"),
            data=json.dumps(data),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(File.objects.count(), nr_files_before)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @patch("core.azure_storage.Blob.delete_blob")
    def test_delete_multiple_files_id_as_string(self, mock_delete_blob):
        """Delete multiple zones at once, id may be integer or string type"""
        # Arrange
        mock_delete_blob.return_value = True
        for i in range(4):
            create_file(f"file_{i}")

        nr_files_before = File.objects.count()
        file1 = File.objects.first()
        file2 = File.objects.last()
        payload = {"ids": [file1.id, str(file2.id)]}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.delete(
            reverse("api-v1:api-v1-files"),
            data=json.dumps(payload),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(File.objects.count(), nr_files_before - len(payload["ids"]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class StudyDeleteTests(APITestCase):
    """Delete single study"""

    @classmethod
    def setUpTestData(cls):
        cls.admin = create_user(staff=True)

    def test_delete_study_used_by_file(self):
        """
        If a study is refered to by a file, then the study can not be deleted
        """
        # Arrange
        study = Study.objects.create(name="study_Foo")
        create_file("testfile", study=study)
        self.client.force_authenticate(user=self.admin)
        nr_studies_before = Study.objects.count()

        # Assume
        self.assertTrue(study.file_set.count())

        # Act
        response = self.client.delete(
            reverse("api-v1:api-v1-study", kwargs={"pk": study.id}),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(Study.objects.count(), nr_studies_before)
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)

    def test_delete_study_without_related_files(self):
        """If a study is not refered to by any file then it can be deleted"""
        # Arrange
        study = Study.objects.create(name="study_Foo")
        self.client.force_authenticate(user=self.admin)

        # Assume
        self.assertFalse(study.file_set.count())

        # Act
        response = self.client.delete(
            reverse("api-v1:api-v1-study", kwargs={"pk": study.id}),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class StudiesDeleteTests(APITestCase):
    """Delete multiple studies"""

    @classmethod
    def setUpTestData(cls):
        cls.admin = create_user(staff=True)

    def test_delete_multiple_studies_some_referred_to(self):
        """
        Delete multiple studies, some of them are referred to by files
        """
        # Arrange
        for i in range(4):
            Study.objects.create(name=f"study_{i}")

        study1 = Study.objects.first()
        create_file("testfile", study=study1)
        nr_studies_before = Study.objects.count()
        payload = {"ids": [study1.id, Study.objects.last().id]}
        self.client.force_authenticate(user=self.admin)

        # Assume
        self.assertTrue(study1.file_set.count())

        # Act
        response = self.client.delete(
            reverse("api-v1:api-v1-studies"),
            data=json.dumps(payload),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(Study.objects.count(), nr_studies_before)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class DeleteChapterTests(APITestCase):
    """
    Tests for the D in CRUD
    Deleting one or more chapters
    """

    @classmethod
    def setUpTestData(cls):
        cls.admin = create_user(staff=True)

    def test_delete_chapter_used_by_study(self):
        """
        If a chapter is refered to by a study, then the chapter can not be deleted
        """
        # Arrange
        chapter = Chapter.objects.create(name="chapter_one")
        Study.objects.create(name="study_Foo", chapter=chapter)
        nr_chapters_before = Chapter.objects.count()
        self.client.force_authenticate(user=self.admin)

        # Assume
        self.assertTrue(chapter.study_set.count())

        # Act
        response = self.client.delete(
            reverse("api-v1:api-v1-chapter", kwargs={"pk": chapter.id}),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(Chapter.objects.count(), nr_chapters_before)
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)


class ZoneDeleteTests(APITestCase):
    @classmethod
    def setUpTestData(cls):
        """Setup a temporary database for testing this class"""
        cls.admin = create_user(staff=True)

    def test_delete_multiple_zones(self):
        """Delete multiple zones at once, id may be integer or string type"""
        # Arrange
        for i in range(4):
            Zone.objects.create(name=f"zone_{i}", published=True)
        nr_zones_before = Zone.objects.count()
        zone1 = Zone.objects.first()
        zone2 = Zone.objects.last()
        payload = {"ids": [int(zone1.id), str(zone2.id)]}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.delete(
            reverse("api-v1:api-v1-zones"),
            data=json.dumps(payload),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(Zone.objects.count(), nr_zones_before - len(payload["ids"]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_multiple_zones_non_existing_id(self):
        """Delete multiple zones at once, incl an id that doesn't exist"""
        # Arrange
        for i in range(4):
            Zone.objects.create(name=f"zone_{i}", published=True)
        nr_zones_before = Zone.objects.count()
        payload = {"ids": [Zone.objects.first().id, 1_234]}
        self.client.force_authenticate(user=self.admin)

        # Act
        response = self.client.delete(
            reverse("api-v1:api-v1-zones"),
            data=json.dumps(payload),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(Zone.objects.count(), nr_zones_before - 1)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_multiple_zones_some_referred_to(self):
        """
        Delete multiple zones, some of them are referred to by files
        """
        # Arrange
        for i in range(4):
            Zone.objects.create(name=f"zone_{i}", published=True)

        zone1 = Zone.objects.first()
        create_file("testfile", zone=zone1)
        nr_zones_before = Zone.objects.count()
        payload = {"ids": [zone1.id, Zone.objects.last().id]}
        self.client.force_authenticate(user=self.admin)

        # Assume
        self.assertTrue(zone1.file_set.count())

        # Act
        response = self.client.delete(
            reverse("api-v1:api-v1-studies"),
            data=json.dumps(payload),
            content_type="application/json",
        )

        # Assert
        self.assertEqual(Zone.objects.count(), nr_zones_before)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
