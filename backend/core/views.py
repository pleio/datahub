import pathlib
from datetime import datetime, timezone

from audit.models import Log
from core.azure_storage import Blob, get_md5_hash
from core.custom_filters import (
    CaseInsensitiveOrderingFilter,
    ConditionalCustomPagination,
    CustomPagination,
    FileFilter,
    StudyFilter,
    ZoneFilter,
)
from core.models import (
    Chapter,
    Classification,
    DatahubUser,
    File,
    FileExtension,
    FileStatusRename,
    Sensor,
    Study,
    Zone,
)
from core.serializers import (
    ChapterSerializer,
    ClassificationSerializer,
    ExtensionSerializer,
    FileDownloadGETSerializer,
    FileGETSerializer,
    FilePOSTSerializer,
    FilePUTSerializer,
    IdsListSerializer,
    SensorSerializer,
    StudyGETSerializer,
    StudyPOSTSerializer,
    ZoneSerializer,
)
from core.tasks import rename_blob
from core.views_helper_methods import (
    file_download_data,
    get_permissions,
    get_queryset_files,
)
from django.conf import settings
from django.contrib.auth import logout
from django.db.models import ProtectedError
from django.shortcuts import get_object_or_404
from django_filters import rest_framework as dr_filters
from rest_framework import filters, generics, status
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView


class HealthView(APIView):
    def get(self, request):
        """Basic check to see whether the API is responsive."""
        return Response({"status": {"success": True}}, status=status.HTTP_200_OK)


@api_view(("GET",))
def logout_view(request):
    """Logout view"""
    if not request.user.is_authenticated:
        return Response(
            {"detail": "You're not logged in."}, status=status.HTTP_400_BAD_REQUEST
        )

    logout(request)
    return Response({"detail": "Successfully logged out."}, status=status.HTTP_200_OK)


class UserInfo(APIView):
    """Retrieve user information"""

    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user = get_object_or_404(DatahubUser, username=request.user.username)
        output = {
            "email": user.email,
            "name": user.name,
            "username": user.username,
            "is_active": user.is_active,
            "is_superadmin": user.is_superadmin,
            "is_staff": user.is_staff,
            "is_superuser": user.is_superuser,
            "external_id": user.external_id,
            "picture": user.picture,
        }
        return Response(output, status=status.HTTP_200_OK)


class CustomDestroyMixin:
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            self.perform_destroy(instance)

            Log(
                user=request.user,
                class_name=self.__class__.__name__,
                function="destroy",
                message=f"Deleted '{instance}'",
            ).save()
            return Response(status=status.HTTP_204_NO_CONTENT)

        except ProtectedError as e:
            Log(
                user=request.user,
                class_name=self.__class__.__name__,
                function="destroy",
                message=f"ProtectedError when deleting '{instance}':\n{e}",
            ).save()

            return Response(
                {
                    "error": f"Some reference to this object '{instance}' exists. "
                    f"Therefore this object can't be deleted."
                },
                status=status.HTTP_409_CONFLICT,
            )


class UnpublishMixin:
    def put(self, request, *args, **kwargs):
        """
        Unpublish multiple objects from a given model.
        This method expects a list of ids
        e.g. request.data = {'ids': [1, 2. 3]}
        where 1, 2, and 3 are the ids of the objects to update
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        given_ids = serializer.validated_data.get("ids", [])
        qs = self.get_queryset().filter(pk__in=given_ids, published=True)
        nr_success = qs.update(
            published=False, date_modified=datetime.now(tz=timezone.utc)
        )
        result = {204: nr_success, 404: len(given_ids) - nr_success}
        return Response(result, status=status.HTTP_200_OK)


class GetAlsoUnpublishedMixin:
    def get_queryset(self, *args, **kwargs):
        """Unpublished objects should only be visible in GET requests
        for admin users that explicitly requested them.
        """
        if self.request.method != "GET":
            return self.model.objects.all()

        also_unpublished = (
            self.request.query_params.get("also_unpublished", "False").lower() == "true"
        )

        if self.request.user.is_staff and also_unpublished:
            return self.model.objects.all()

        # A GET request and no permissions to see unpublished objects, or no
        # unpublished objects were requested
        return self.model.objects.filter(published=True)


class TotalsView(APIView):
    permission_classes = [IsAdminUser]

    def get(self, request):
        return Response(
            {
                "published_zones": Zone.objects.filter(published=True).count(),
                "published_studies": Study.objects.filter(published=True).count(),
                "published_files": File.objects.filter(published=True).count(),
                "unprocessed_files": File.objects.filter(processed=False).count(),
            },
            status=status.HTTP_200_OK,
        )


class SensorList(generics.ListAPIView):
    queryset = Sensor.objects.all()
    serializer_class = SensorSerializer
    pagination_class = CustomPagination
    permission_classes = get_permissions()


class ChapterList(generics.ListCreateAPIView):
    """
    A Chapter is a category under which different studies are
    grouped together
    """

    queryset = Chapter.objects.all()
    serializer_class = ChapterSerializer
    pagination_class = ConditionalCustomPagination
    permission_classes = get_permissions()
    filter_backends = (
        filters.SearchFilter,
        CaseInsensitiveOrderingFilter,
    )
    search_fields = [
        "name",
    ]
    ordering_fields = [
        "name",
        "date_created",
        "date_modified",
    ]


class ChapterDetail(CustomDestroyMixin, generics.RetrieveUpdateDestroyAPIView):
    """
    A Chapter is a category under which different studies are
    grouped together
    """

    serializer_class = ChapterSerializer
    queryset = Chapter.objects.all()
    permission_classes = get_permissions()


class CreateFile(generics.CreateAPIView):
    """
    Endpoint for events coming from Azure Storage
    """

    # https://stackoverflow.com/questions/47551989/how-to-rewrite-the-apiviews-post-method
    serializer_class = FilePOSTSerializer

    def post(self, request):
        """
        Handle post requests from Azure Event Grid. Those post requests
        are automatically triggered after a file is uploaded to the
        Azure Storage (the event).
        """

        def log(msg):
            Log(
                class_name=self.__class__.__name__,
                function="post",
                message=msg,
            ).save()

        try:
            request_data = request.data[0]
            data = request_data["data"]
            request_topic = request_data["topic"]
        except KeyError as e:
            log(f"KeyError: {e}")
            return Response({"message": "KeyError"}, status=status.HTTP_400_BAD_REQUEST)

        if not self._authenticated_request(request_topic):
            log("Unauthenticated request")
            return Response(
                {"message": "Request is not authenticated."},
                status=status.HTTP_400_BAD_REQUEST,
            )

        validationcode = data.get("validationCode")
        if validationcode:
            """
            https://learn.microsoft.com/en-us/azure/event-grid/webhook-event-delivery
            TL;DR For synchronous handshake the validationcode should be returned
            """
            return Response(
                {"validationResponse": validationcode}, status=status.HTTP_200_OK
            )

        request_type = data.get("api")
        if request_type not in {"PutBlob", "PutBlockList"}:
            log(f"No file created for request type: '{request_type}'")
            return Response({"message": "No file created"}, status=status.HTTP_200_OK)

        log(f"Request data coming from Azure Event Grid: {request_data}")
        file = File.objects.create(**self._convert(data))
        log(f"File created. Name: '{file.name}' Url: '{file.url}'")
        return Response({"message": "File created"}, status=status.HTTP_201_CREATED)

    def _authenticated_request(self, request_topic):
        """
        Returns true if the request contains the correct subscription id
        """
        subscription_id = settings.AZURE_SUBSCRIPTION_ID
        return subscription_id and subscription_id.strip() in request_topic

    def _convert(self, data):
        """
        Converts raw data from the response into data that can be used to create a file
        """
        url = data["url"]
        name = pathlib.Path(url).name
        return {
            "name": name,
            "size": data["contentLength"],
            "url": url,
            "md5_hash": get_md5_hash(url),
        }


class FileList(UnpublishMixin, generics.ListAPIView):
    """
    A File here is the metadata of a file (the file itself
    is stored somewhere else)
    """

    def get_serializer_class(self):
        if self.request.method in ("DELETE", "PUT"):
            return IdsListSerializer
        return FileGETSerializer

    def get_queryset(self):
        return get_queryset_files(self.request)

    def delete(self, request, *args, **kwargs):
        """
        This method expects a list of ids e.g.
        request.data = {'ids': [1, 2. 3]}
        where 1, 2, and 3 are the ids of the objects to delete
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        given_ids = serializer.validated_data.get("ids", [])
        Log(
            user=request.user,
            class_name=self.__class__.__name__,
            function="destroy",
            message=f"Request to delete files with ids '{given_ids}'",
        ).save()

        for pk in given_ids:
            file = get_object_or_404(File, id=pk)
            blob_successfull_deleted = Blob(file).delete_blob()
            if not blob_successfull_deleted:
                continue
            file.delete()
            Log(
                user=request.user,
                class_name=self.__class__.__name__,
                function="delete",
                message=f"Deleted '{file}' with url '{file.url}'",
            ).save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    pagination_class = CustomPagination
    permission_classes = get_permissions()
    filter_backends = (
        filters.SearchFilter,
        CaseInsensitiveOrderingFilter,
        dr_filters.DjangoFilterBackend,
    )
    search_fields = [
        "name",
        "classification__name",
        "description",
        "extension__name",
        "sensor__name",
        "study__name",
    ]
    ordering_fields = [
        "name",
        "zone__name",
        "classification__name",
        "extension__name",
        "sensor__name",
        "size",
        "study__name",
        "study__chapter__name",
        "date_created",
        "date_modified",
    ]
    filterset_class = FileFilter

    def filter_queryset(self, *args, **kwargs):
        qs = super().filter_queryset(*args, **kwargs)
        requested_classifications = self.request.query_params.getlist("classification")
        if requested_classifications:
            files = File.objects.filter(
                classification__id__in=requested_classifications
            ).distinct()
            qs = qs.filter(pk__in=files)

        requested_zones = self.request.query_params.getlist("zone")
        if requested_zones:
            files = File.objects.filter(zone__id__in=requested_zones).distinct()
            qs = qs.filter(pk__in=files)
        return qs


class FileDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    A File here is the metadata of a file (the file itself
    is stored somewhere else)
    """

    def get_queryset(self):
        return get_queryset_files(self.request)

    def get_serializer_class(self):
        if self.request.method == "PUT":
            return FilePUTSerializer
        return FileGETSerializer

    def put(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.status_rename == FileStatusRename.IN_PROGRESS.value:
            return Response(
                {"message": "Renaming in progress"}, status=status.HTTP_423_LOCKED
            )

        # Save original name to be able to restore if necessary
        original_name = instance.name

        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        desired_name = serializer.validated_data.get("name", instance.name)

        if desired_name != original_name:
            instance.status_rename = FileStatusRename.IN_PROGRESS.value
            instance.save()
            rename_blob.delay(instance.id, original_name, desired_name)
        else:
            # Reset rename status to None if no rename is happening
            instance.status_rename = None
            instance.save()

        updated_instance = serializer.update(instance, serializer.validated_data)
        response_data = self.get_serializer(updated_instance).data
        return Response(response_data, status=status.HTTP_200_OK)

    def destroy(self, request, pk, *args, **kwargs):
        file = get_object_or_404(File, pk=pk)
        Log(
            user=request.user,
            class_name=self.__class__.__name__,
            function="destroy",
            message=f"request to delete '{file}' with url '{file.url}'",
        ).save()
        success = Blob(file).delete_blob()
        if not success:
            return Response(
                {"error": f"Error when deleting '{file}' from storage"},
                status.HTTP_400_BAD_REQUEST,
            )
        super().perform_destroy(file)
        return Response({}, status.HTTP_204_NO_CONTENT)

    permission_classes = get_permissions()


class FileDownload(generics.RetrieveAPIView):
    def get_serializer_class(self):
        if self.request.method == "GET":
            return FileDownloadGETSerializer
        return None

    def get_queryset(self):
        return get_queryset_files(self.request)

    def get(self, request, pk, *args, **kwargs):
        file = get_object_or_404(File, pk=pk)
        if file.status_rename == FileStatusRename.IN_PROGRESS.value:
            return Response({"Renaming in progress"}, status=status.HTTP_423_LOCKED)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        result = file_download_data(file, request.session.session_key)

        return Response(result, status=status.HTTP_200_OK)

    permission_classes = get_permissions()


class FilesDownload(generics.ListAPIView):
    def get_queryset(self, *args):
        return get_queryset_files(self.request)

    def get(self, request, *args, **kwargs):
        input_ids = request.query_params.get("ids", "")
        try:
            ids = [int(n) for n in input_ids.split(",")]
        except (ValueError,):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        files = File.objects.filter(id__in=ids)

        result = []
        for file in files:
            if file.status_rename == FileStatusRename.IN_PROGRESS.value:
                continue
            result.append(file_download_data(file, request.session.session_key))
        return Response(result, status=status.HTTP_200_OK)

    permission_classes = get_permissions()


class StudyList(GetAlsoUnpublishedMixin, generics.ListCreateAPIView):
    """
    A Study is a single study that the RVO requested
    """

    def get_serializer_class(self):
        if self.request.method == "POST":
            return StudyPOSTSerializer
        return StudyGETSerializer

    def delete(self, request, *args, **kwargs):
        # This method expects a list of ids
        # e.g. request.data = {'ids': [1, 2. 3]}
        # where 1, 2, and 3 are the ids of the objects to delete
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        given_ids = serializer.validated_data.get("ids", [])

        Study.objects.filter(pk__in=given_ids).delete()
        Log(
            user=request.user,
            class_name=self.__class__.__name__,
            function="delete",
            message=f"Deleted: '{given_ids=}'",
        ).save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    model = Study
    pagination_class = ConditionalCustomPagination
    permission_classes = get_permissions()
    filter_backends = (
        filters.SearchFilter,
        CaseInsensitiveOrderingFilter,
        dr_filters.DjangoFilterBackend,
    )
    search_fields = [
        "name",
        "chapter__name",
    ]
    ordering_fields = [
        "name",
        "date_created",
        "date_modified",
        "published",
        "chapter__name",
    ]
    filterset_class = StudyFilter


class StudyDetail(
    GetAlsoUnpublishedMixin, CustomDestroyMixin, generics.RetrieveUpdateDestroyAPIView
):
    """
    A Study is a single study that the RVO requested
    """

    def get_serializer_class(self):
        if self.request.method == "PUT":
            return StudyPOSTSerializer
        return StudyGETSerializer

    model = Study
    permission_classes = get_permissions()


class ZoneList(GetAlsoUnpublishedMixin, UnpublishMixin, generics.ListCreateAPIView):
    """
    A Zone is a demarcated area in which a wind farm (will be) realised.
    """

    def get_serializer_class(self):
        if self.request.method == "DELETE":
            return IdsListSerializer
        return ZoneSerializer

    def delete(self, request, *args, **kwargs):
        # This method expects a list of ids
        # e.g. request.data = {'ids': [1, 2. 3]}
        # where 1, 2, and 3 are the ids of the objects to delete
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        given_ids = serializer.validated_data.get("ids", [])

        Zone.objects.filter(pk__in=given_ids).delete()
        Log(
            user=request.user,
            class_name=self.__class__.__name__,
            function="delete",
            message=f"Deleted: '{given_ids=}'",
        ).save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    model = Zone
    pagination_class = ConditionalCustomPagination
    permission_classes = get_permissions()
    filter_backends = (
        filters.SearchFilter,
        CaseInsensitiveOrderingFilter,
        dr_filters.DjangoFilterBackend,
    )
    search_fields = [
        "name",
    ]
    ordering_fields = [
        "name",
        "date_created",
        "date_modified",
        "published",
    ]
    filterset_class = ZoneFilter


class ZoneDetail(
    GetAlsoUnpublishedMixin, CustomDestroyMixin, generics.RetrieveUpdateDestroyAPIView
):
    """
    A Zone is a demarcated area in which a wind farm (will be) realised.
    """

    serializer_class = ZoneSerializer
    permission_classes = get_permissions()
    model = Zone


class ClassificationList(generics.ListAPIView):
    """
    A Classification helps to identify the role of a file within a study
    """

    queryset = Classification.objects.all()
    pagination_class = ConditionalCustomPagination
    serializer_class = ClassificationSerializer
    permission_classes = get_permissions()


class ExtensionList(generics.ListAPIView):
    queryset = FileExtension.objects.all()
    serializer_class = ExtensionSerializer
    pagination_class = ConditionalCustomPagination
    permission_classes = get_permissions()
    filter_backends = (
        filters.SearchFilter,
        CaseInsensitiveOrderingFilter,
    )
    search_fields = [
        "name",
    ]
    ordering_fields = [
        "name",
        "date_created",
        "date_modified",
    ]
