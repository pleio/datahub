import pathlib
from enum import Enum

from django.contrib.auth.models import AbstractUser
from django.core.exceptions import MultipleObjectsReturned, ValidationError
from django.db import models


class FileStatusRename(Enum):
    FAILED = "FAILED"
    IN_PROGRESS = "IN_PROGRESS"
    SUCCESS = "SUCCESS"


class DatahubUser(AbstractUser):
    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    username = models.CharField(max_length=100, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_superadmin = models.BooleanField(default=False)

    external_id = models.CharField(max_length=50, unique=True, blank=True, null=True)
    picture = models.URLField(blank=True, null=True)

    # Field for login
    USERNAME_FIELD = "email"

    # Field for command createsuperuser
    REQUIRED_FIELDS = ["username"]

    def __str__(self):
        return f"{self.email}"


class CommonBase(models.Model):
    class Meta:
        abstract = True
        ordering = ["-date_modified"]

    name = models.CharField(max_length=100)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.name)


class Chapter(CommonBase):
    pass


class Classification(CommonBase):
    pass


class Sensor(CommonBase):
    pass


class FileExtension(CommonBase):
    pass


class Zone(CommonBase):
    published = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.name}"


class Study(CommonBase):
    chapter = models.ForeignKey(
        Chapter, on_delete=models.PROTECT, null=True, blank=True
    )
    published = models.BooleanField(default=False)

    class Meta(CommonBase.Meta):
        verbose_name_plural = "studies"

    def __str__(self):
        return f"{self.name}"


class File(CommonBase):
    classification = models.ForeignKey(
        Classification, on_delete=models.PROTECT, null=True, blank=True
    )
    description = models.TextField(max_length=500, null=True, blank=True)
    extension = models.ForeignKey(
        FileExtension, on_delete=models.PROTECT, null=True, blank=True
    )
    published = models.BooleanField(default=False)
    sensor = models.ForeignKey(Sensor, on_delete=models.PROTECT, null=True, blank=True)
    study = models.ForeignKey(Study, on_delete=models.PROTECT, null=True, blank=True)
    size = models.PositiveBigIntegerField(null=True, blank=True)
    nr_downloads = models.PositiveBigIntegerField(default=0, null=True, blank=True)
    processed = models.BooleanField(default=False)
    url = models.CharField(max_length=200, null=True, blank=True)
    md5_hash = models.CharField(max_length=100, null=True, blank=True)
    zone = models.ForeignKey(Zone, on_delete=models.PROTECT, null=True, blank=True)

    STATUS_RENAME_CHOICES = [(status.value, status.name) for status in FileStatusRename]

    status_rename = models.CharField(
        max_length=15,
        choices=STATUS_RENAME_CHOICES,
        default=None,
        null=True,
        blank=True,
    )

    # This field is added to ensure backwards compatibility with files that were created
    # when there was still a soft-delete. This field is supposed to be 'False'
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.name}"

    def published_processed_validator(self):
        if self.published and not self.processed:
            raise ValidationError("Published files must be processed.")

    def set_extension(self):
        if not self.url:
            return
        extension_name = pathlib.Path(self.url).suffix.upper()[1:]
        try:
            extension, _ = FileExtension.objects.get_or_create(name=extension_name)
        except MultipleObjectsReturned:
            # Note that this should not be possible, but somehow it happens
            extension = FileExtension.objects.filter(name=extension_name).first()
        self.extension = extension

    def save(self, *args, **kwargs):
        self.published_processed_validator()
        self.set_extension()
        super().save(*args, **kwargs)
