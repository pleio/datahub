from core.models import DatahubUser
from django.db.models import Q
from mozilla_django_oidc.auth import OIDCAuthenticationBackend


class OIDCAuthBackend(OIDCAuthenticationBackend):
    def filter_users_by_claims(self, claims):
        external_id = claims.get("sub")
        email = claims.get("email")

        if not external_id or not email:
            return DatahubUser.objects.none()

        return DatahubUser.objects.filter(
            Q(external_id__iexact=external_id) | Q(email__iexact=email)
        )

    def create_user(self, claims):
        user = DatahubUser.objects.create_user(
            username=claims.get("email"),
            name=claims.get("name"),
            email=claims.get("email"),
            picture=claims.get("picture", None),
            password=None,
            external_id=claims.get("sub"),
            is_superadmin=claims.get("is_admin", False),
        )

        return user

    def update_user(self, user, claims):
        user.external_id = claims.get("sub")
        user.email = claims.get("email")

        # if user profile picture file exists, do not change to
        # picture from account
        if claims.get("picture"):
            user.picture = claims.get("picture")
        else:
            user.picture = None

        # Get and set superadmin
        if claims.get("is_admin"):
            user.is_superadmin = True
            user.is_staff = True
        else:
            user.is_superadmin = False

        if user.is_superadmin:
            user.is_staff = True
            user.is_superuser = True

        user.save()
        return user
