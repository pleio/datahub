from core.models import File, Study, Zone
from django.conf import settings
from django.db.models.functions import Lower
from django.utils.datastructures import MultiValueDictKeyError
from django_filters import rest_framework as dr_filters
from rest_framework import filters, pagination


class CaseInsensitiveOrderingFilter(filters.OrderingFilter):
    """
    Ordering is set by a comma delimited ?ordering=... query parameter.
    Descending ordering is done by prefixing with a '-'

    This customization makes the ordering case insensitive.
    Note that this doesn't work for all field types, and therefore
    certain (hardcoded) fields are handled differently.
    """

    def filter_queryset(self, request, queryset, view):
        """
        Returns the given queryset with custom ordering.
        Implemented here:
        1. case insensitive ordering
        """
        ordering = self.get_ordering(request, queryset, view)

        if not ordering:
            return queryset

        new_ordering = []
        non_string_fields = {
            "id",
            "size",
            "date_created",
            "date_modified",
            "processed",
            "is_deleted",
            "deleted_at",
            "published",
        }

        for field in ordering:
            # Explicitly except non-string fields
            if {field, field[1:]}.intersection(non_string_fields):
                new_ordering.append(field)

            elif field.startswith("-"):
                new_ordering.append(Lower(field[1:]).desc())
            else:
                new_ordering.append(Lower(field).asc())

        return queryset.order_by(*new_ordering)


class FileFilter(dr_filters.FilterSet):
    modified_after = dr_filters.DateTimeFilter(
        field_name="date_modified", lookup_expr="gte"
    )
    modified_before = dr_filters.DateTimeFilter(
        field_name="date_modified", lookup_expr="lte"
    )

    class Meta:
        model = File
        fields = [
            "sensor",
            "study",
            "study__chapter",
            "published",
            "modified_after",
            "modified_before",
            "extension",
        ]


class ZoneFilter(dr_filters.FilterSet):
    modified_after = dr_filters.DateTimeFilter(
        field_name="date_modified", lookup_expr="gte"
    )
    modified_before = dr_filters.DateTimeFilter(
        field_name="date_modified", lookup_expr="lte"
    )

    class Meta:
        model = Zone
        fields = ["published", "modified_after", "modified_before"]


class StudyFilter(dr_filters.FilterSet):
    modified_after = dr_filters.DateTimeFilter(
        field_name="date_modified", lookup_expr="gte"
    )
    modified_before = dr_filters.DateTimeFilter(
        field_name="date_modified", lookup_expr="lte"
    )

    class Meta:
        model = Study
        fields = ["published", "chapter", "modified_after", "modified_before"]


class CustomPagination(pagination.LimitOffsetPagination):
    """
    Pagination is set by the ?limit=...&offset=... query parameters.

    This custom pagination restricts the limit parameter.
    """

    default_limit = settings.DEFAULT_PAGINATION
    min_limit = settings.MIN_PAGINATION
    max_limit = settings.MAX_PAGINATION

    def validate_limits(self, request):
        try:
            given_limit = int(request.query_params[self.limit_query_param])
        except (
            MultiValueDictKeyError,
            ValueError,
        ):
            given_limit = self.default_limit

        if given_limit > self.max_limit:
            raise ValueError(f"Please use a limit less than {self.max_limit} (given: {given_limit})")
        elif given_limit < self.min_limit:
            raise ValueError(f"Please use a limit above {self.min_limit} (given: {given_limit})")
        return True

    def get_limit(self, request):
        if self.validate_limits(request):
            return super().get_limit(request)

    def get_limit_default(self, request):
        """
        This method enables child classes to skip the get_limit()
        implementation in this 'middle' class
        """
        return super().get_limit(request)


class ConditionalCustomPagination(CustomPagination):
    """
    Restricts the limit parameter off the pagination, unless
    pagination=false is requested.
    """

    def get_limit(self, request):
        if self._pagination_off(request):
            self.default_limit = 100_000  # Basically 'unlimited'
            return self.get_limit_default(request)
        return super().get_limit(request)

    def _pagination_off(self, request):
        """
        Returns True if pagination=false is requested and authorized
        """
        return (
            request.query_params.get("pagination", "True").lower() == "false"
            and request.user.is_staff
        )
