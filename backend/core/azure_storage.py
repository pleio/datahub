from datetime import datetime, timedelta
from typing import Union

from audit.models import Log
from azure.core.exceptions import (
    ClientAuthenticationError,
    ResourceExistsError,
    ResourceNotFoundError,
    ServiceRequestError,
)
from azure.storage.blob import (
    BlobClient,
    BlobSasPermissions,
    BlobServiceClient,
    BlobType,
    generate_blob_sas,
)
from django.conf import settings


class Blob:
    """Interaction with 'blobs' in Azure Storage Account."""

    _account_name = settings.AZURE_ACCOUNT_NAME
    _account_key = settings.AZURE_ACCOUNT_KEY
    _container_name = settings.AZURE_CONTAINER_NAME

    def __init__(self, file):
        if not all(
            (
                self._account_name,
                self._account_key,
                self._container_name,
            )
        ):
            raise ValueError("Some credentials for Azure Storage are missing")

        # 'path' is the path in container + filename 'year/month/file.txt'
        # Example: 2023/09/foo.txt
        self.path = file.url.split(self._container_name)[-1][1:]
        self._account_url = f"https://{self._account_name}.blob.core.windows.net"
        self.url = f"{self._account_url}/{self._container_name}/{self.path}"

    def create_download_command(self) -> str:
        """Create CLI download command for Azures AzCopy"""
        SAS_token = self._get_SAS_token()
        if SAS_token:
            return f'azcopy copy "{self.url}?{SAS_token}" .'
        return ""

    def create_download_url(self) -> str:
        """Create url to download directly (without use of AzCopy)"""
        SAS_token = self._get_SAS_token()
        return f"{self.url}?{SAS_token}"

    def copy_blob(self, destination_path: str) -> bool:
        """Copy the blob at 'self.url' to the given 'destination_path'"""
        blob_service_client = self._get_blob_service_client()
        destination_container_client = blob_service_client.get_container_client(
            self._container_name
        )
        destination_blob_client = destination_container_client.get_blob_client(
            destination_path
        )
        if self._destination_exists(destination_blob_client):
            Log(
                class_name=self.__class__.__name__,
                function="copy_blob",
                message=f"Blob already exists at destination: {destination_path}",
            ).save()
            return False
        try:
            destination_blob_client.start_copy_from_url(self.url)
            return True
        except (
            ResourceNotFoundError,
            ClientAuthenticationError,
            ResourceExistsError,
        ) as e:
            Log(
                class_name=self.__class__.__name__,
                function="copy_blob",
                message=f"{destination_path=} Error: {e}",
            ).save()
            return False

    def delete_blob(self) -> bool:
        """Delete the blob from Azure storage"""
        blob_service_client = self._get_blob_service_client()
        if not blob_service_client:
            return False
        try:
            blob_client = blob_service_client.get_blob_client(
                container=self._container_name, blob=self.path
            )
            blob_client.delete_blob()
            return True
        except (
            ResourceNotFoundError,
            ClientAuthenticationError,
            ResourceExistsError,
        ) as e:
            Log(
                class_name=self.__class__.__name__, function="delete_blob", message=e
            ).save()
            return False

    def _get_SAS_token(self, days: int = 7) -> str:
        """Returns a SAS token (Shared Access Signature) as string."""

        def log_error(msg):
            Log(
                class_name=self.__class__.__name__,
                function="_get_SAS_token",
                message=msg,
            ).save()

        try:
            if not callable(generate_blob_sas):
                log_error("'generate_blob_sas' must be a callable")
                return ""
            if not all(
                hasattr(self, attr)
                for attr in (
                    "_account_name",
                    "_account_key",
                    "_container_name",
                    "path",
                )
            ):
                log_error(f"Missing one or more required attributes on {self}.")
                return ""

            sas = generate_blob_sas(
                account_name=self._account_name,
                account_key=self._account_key,
                container_name=self._container_name,
                blob_name=self.path,
                permission=BlobSasPermissions(read=True),
                start=datetime.utcnow() - timedelta(hours=1),
                expiry=datetime.utcnow() + timedelta(days=days),
            )
            return sas

        except (
            AttributeError,
            ClientAuthenticationError,
            ConnectionError,
            FileNotFoundError,
            PermissionError,
            TypeError,
            ValueError,
        ) as e:
            log_error(e)
        return ""

    def _get_blob_service_client(self) -> Union[BlobServiceClient, None]:
        """Returns an initialized BlobServiceClient instance, or None"""
        try:
            return BlobServiceClient(
                account_url=self._account_url, credential=self._account_key
            )
        except (
            ResourceNotFoundError,
            ClientAuthenticationError,
            ResourceExistsError,
        ) as e:
            Log(
                class_name=self.__class__.__name__,
                function="_get_blob_service_client",
                message=e,
            ).save()
        return None

    def _destination_exists(self, destination_blob_client: str) -> bool:
        """Checks if the given 'destination_blob_client' points to a location on
        Azure that is already taken. A 'ResourceNotFoundError' indicates there is no
        blob yet at the target location.
        """
        try:
            destination_blob_properties = destination_blob_client.get_blob_properties()
            return destination_blob_properties.blob_type == BlobType.BlockBlob
        except ResourceNotFoundError:
            return False


def get_md5_hash(blob_url):
    """
    When a file is uploaded via AzCopy to the Azure Storage, a md5
    hash can be created and added. This is done with the --put-md5 flag
    and results in a 64-bit representation of the hash.
    This method fetches that md5 hash and returns the hexadecimal
    representation as a string.
    """
    blob = BlobClient.from_blob_url(blob_url)
    try:
        blob_properties = blob.get_blob_properties()
    except (ResourceNotFoundError, ServiceRequestError) as e:
        Log(function="get_md5_hash", message=f"{blob_url=} Error: {e}").save()
        return ""
    try:
        md5_hash_base64 = blob_properties["content_settings"]["content_md5"]
    except TypeError as e:
        Log(function="get_md5_hash", message=f"{blob_url=} Error: {e}").save()
        return ""
    if md5_hash_base64 is None:
        return ""

    return bytes(md5_hash_base64).hex()


if __name__ == "__main__":
    pass
