"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from core.views import CreateFile
from django.conf import settings
from django.contrib import admin
from django.shortcuts import render
from django.urls import include, path, re_path
from django.views.generic.base import TemplateView

admin.site.site_header = (
    "Datahub Administration"
    f" - {settings.ENVIRONMENT}"
    f" - {settings.CI_COMMIT_TAG}"
    f" - Azure-account: '{settings.AZURE_ACCOUNT_NAME}'"
    f" - Container: '{settings.AZURE_CONTAINER_NAME}'"
)


# frontend renderer
def index_view(request, *args, **kwargs):
    return render(request, "build/index.html")


# frontend routes
urlpatterns = [
    path(f"{settings.DJANGO_ADMIN_URL}/", admin.site.urls),
    path(
        "robots.txt",
        TemplateView.as_view(
            template_name="build/robots.txt", content_type="text/plain"
        ),
    ),
    path(
        ".well-known/security.txt",
        TemplateView.as_view(
            template_name="build/.well-known/security.txt", content_type="text/plain"
        ),
    ),
    path(
        "manifest.json",
        TemplateView.as_view(
            template_name="build/manifest.json", content_type="application/json"
        ),
    ),
    path(
        "asset-manifest.json",
        TemplateView.as_view(
            template_name="build/asset-manifest.json", content_type="application/json"
        ),
    ),
    path("api/v1/", include(("core.urls", "api-v1"), namespace="api-v1")),
    path("api/oidc/", include("mozilla_django_oidc.urls")),
    path("api/updates/", CreateFile.as_view(), name="createfile"),
    path("", index_view, name="index"),
    re_path(r"^[a-z]{2}/", index_view, name="index"),
]

if settings.DEBUG:
    import debug_toolbar
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    urlpatterns.append(path("__debug__/", include(debug_toolbar.urls)))
    urlpatterns += staticfiles_urlpatterns()
