```mermaid
erDiagram
  zone {
    column id
    column name
    column date_created
    column date_modified
    column published
  }
  
  study {
    column id
    column name
    column date_created
    column date_modified
    column chapter_id
    column published
  }
  study }o--|| chapter : chapter_id
  
  file {
    column id
    column name
    column date_created
    column date_modified
    column description
    column file_size
    column processed
    column is_deleted
    column published
    column study_id
    column classification_id
    column sensor_id
    column file_extension_id
  }
  file }o--|| study : study_id
  file }o--|| classification : classification_id
  file }o--|| sensor : sensor_id
  file }o--|| file_extension: file_extension_id
  file }o--|| zone: zone_id
  
  chapter {
    column id
    column name
    column date_created
    column date_modified
  }

  classification {
    column id
    column name
    column date_created
    column date_modified
  }

  sensor {
    column id
    column name
    column date_created
    column date_modified
  }

  file_extension {
    column name
    column date_created
    column date_modified
  }
```


## Zone:
studies:Study[]  
name:string  
storagePath:string  
published:bool  

---

## Study:
zone:Zone  
files:File[]  
chapter:Enum(“general_info”, “obstructions”, “soil”, “wind_water”)  
name:string  
published:bool  

---


## File:
study:?Study  
path:string  
filename:string  
extension:string  
size:number  
dateCreated:date  
dateModified:date  
classification:Enum ("raw", "processed", "report")  
sensor:?Enum[] ("multibeam echo sounder", "sidescan sonar", "magnetometer") published:bool  
