.DEFAULT_GOAL := help

SSO_CONTAINER := sso
MKFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
CURRENT_DIR := $(dir $(MKFILE_PATH))

# Define components for container names
DOCKER_COMPOSE_VERSION := $(shell docker-compose --version | grep -o "v2.")
ifeq (${DOCKER_COMPOSE_VERSION}, v2.)
    SEP := -
else
	SEP := _
endif
DEV_PREFIX := dev
ACC_PREFIX := acc
BACKEND_CONTAINER := backend-datahub
FRONTEND_CONTAINER := frontend-datahub

# Construct container names
DEV_BACKEND_CONTAINER="${DEV_PREFIX}${SEP}${BACKEND_CONTAINER}${SEP}1"
DEV_FRONTEND_CONTAINER="${DEV_PREFIX}${SEP}${FRONTEND_CONTAINER}"
ACC_BACKEND_CONTAINER="${ACC_PREFIX}${SEP}${BACKEND_CONTAINER}${SEP}1"
ACC_FRONTEND_CONTAINER="${ACC_PREFIX}${SEP}${FRONTEND_CONTAINER}"

BACKEND_HOST=$(shell printenv | grep DATAHUB_ALLOWED_HOSTS | cut -d "=" -f2 | cut -d "," -f1)
REVERSE_PROXY_STATIC_LOCATION="/var/www/$(BACKEND_HOST)"

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

.PHONY: frontend-local-build
frontend-local-build: ## On a local test system to deploy a new build version to Django
	@cd frontend && yarn && yarn run build && cp -r build ../backend/frontend_build/ && cd ..

.PHONY: frontend-dev-build
frontend-dev-build: ## Makes the static files for the frontend and adds them to the backend project
	@docker rmi ${DEV_FRONTEND_CONTAINER} || true
	@docker run --rm -ti -v $(CURRENT_DIR)/frontend:/tmp  ubuntu:18.04 bash -c "rm -rf /tmp/node_modules" || true
	@docker build -t ${DEV_FRONTEND_CONTAINER} -f frontend/Dockerfile .
	@docker run --rm --volume $(CURRENT_DIR)/frontend/:/code --name ${DEV_FRONTEND_CONTAINER} ${DEV_FRONTEND_CONTAINER}  bash -c "yarn && yarn run build"
	@docker run --rm -t -v $(CURRENT_DIR)/frontend:/opt/frontend -v $(CURRENT_DIR)/backend:/opt/backend ubuntu:18.04 bash -c "cp -r /opt/frontend/build /opt/backend/frontend_build/"
	@docker run --rm -t -v $(CURRENT_DIR)/backend:/opt/backend ubuntu:18.04 bash -c "chmod -R 777 /opt/backend/frontend_build"

.PHONY: frontend-dev
frontend-dev: clean-frontend-dev ## Runs a development server on port 3000
	@docker build -t ${DEV_FRONTEND_CONTAINER} -f frontend/Dockerfile .
	@docker run --rm -d -p 3000:3000 -v ${CURRENT_DIR}/frontend:/code --name ${DEV_FRONTEND_CONTAINER} ${DEV_FRONTEND_CONTAINER} bash -c "yarn && yarn start"

.PHONY: clean-frontend-dev
clean-frontend-dev: ## Cleans the frontend development server
	@docker stop ${DEV_FRONTEND_CONTAINER} || true
	@docker rm ${DEV_FRONTEND_CONTAINER} || true
	# Below may not be required when switching from persistent volumes
	# Left here for legacy, makes sure any leftovers on the development server are picked up
	@docker run --rm -t -v $(CURRENT_DIR)/frontend:/tmp  ubuntu:18.04 bash -c "rm -rf /tmp/node_modules" || true

.PHONY: run-dev
run-dev: clean-dev frontend-dev-build ## Cleans, builds and runs the software on the development environment 
	@docker-compose -p ${DEV_PREFIX} up --build -d
	# Below may not be required when switching from persistent volumes
	# Left here for legacy, makes sure any leftovers on the development server are picked up
	@docker run --rm -t -v $(CURRENT_DIR)/frontend:/tmp  ubuntu:18.04 bash -c "rm -rf /tmp/node_modules" || true
	@docker run --rm -t -v $(CURRENT_DIR)/backend:/opt/backend ubuntu:18.04 bash -c "rm -r /opt/backend/frontend_build/build" || true

.PHONY: clean-dev
clean-dev: ## Cleans the software from the DEVELOPMENT environment
	@docker-compose -p ${DEV_PREFIX} down || true

.PHONY: frontend-build
frontend-build: ## Builds the frontend for an acceptance environment
	@docker rmi $(ACC_FRONTEND_CONTAINER) || true
	@docker volume rm $(ACC_FRONTEND_CONTAINER) || true
	@docker build -t $(ACC_FRONTEND_CONTAINER) -f frontend/Dockerfile .
	@docker volume create $(ACC_FRONTEND_CONTAINER)
	@docker run --rm --volume $(ACC_FRONTEND_CONTAINER):/code --name ${ACC_FRONTEND_CONTAINER} ${ACC_FRONTEND_CONTAINER}  bash -c "yarn && yarn run build"
	@docker run --rm -t -v $(ACC_FRONTEND_CONTAINER):/opt/frontend -v $(CURRENT_DIR)/backend:/opt/backend ubuntu:18.04 bash -c "cp -r /opt/frontend/build /opt/backend/frontend_build/"
	@docker run --rm -t -v $(CURRENT_DIR)/backend:/opt/backend ubuntu:18.04 bash -c "chmod -R 777 /opt/backend/frontend_build"

.PHONY: run-acc
run-acc: clean-acc frontend-build ## Cleans, builds and runs the software on the acceptance environment
	# Spawn acceptance
	@docker-compose -p ${ACC_PREFIX} up --build -d
	# Wait for static folder to be intialized
	@docker exec -t $(ACC_BACKEND_CONTAINER) bash -c 'i=0; until ls /code/static; do echo "Waiting for static.."; ((i+=1)); sleep 1; if [[ "$i" -gt 10 ]]; then exit 1; fi; done'
	@docker cp $(ACC_BACKEND_CONTAINER):/code/static tmp/
	@docker run --rm -t -d --name acc-cache -v $(CURRENT_DIR)/tmp:/tmp/acc-cache -v $(REVERSE_PROXY_STATIC_LOCATION):/tmp/www ubuntu:20.04 bash -c "tail -f /dev/null"
	@docker exec -t acc-cache bash -c "cp -r /tmp/acc-cache/static /tmp/www"
	@docker stop acc-cache || true
	@docker volume rm acc-cache || true

.PHONY: clean-acc
clean-acc: ## Cleans the software from the ACCEPTANCE environment
	@docker-compose -p ${ACC_PREFIX} down || true

.PHONY: logs-dev
logs-dev: ## Shows the logs of the DEVELOPMENT environment; CTRL+C to exit
	@docker-compose -p ${DEV_PREFIX} logs -f

.PHONY: sso-dev
sso-dev: ## Creates a development SSO with RSA256 on port 8080 w/ user admin and password admin
	@docker run --rm -d -p 8080:8080 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin --name ${SSO_CONTAINER} quay.io/keycloak/keycloak:19.0.3 start-dev

.PHONY: clean-sso-dev
clean-sso-dev: ## Cleans the 
	@docker stop ${SSO_CONTAINER} || true
	@docker rm ${SSO_CONTAINER} || true

.PHONY: purge
purge: ## WARNING! Use this with care. It will stop and remove all containers, volumes, networks, etc
	@docker stop $(docker ps -aq) || true
	@docker rm $(docker ps -aq) || true
	@docker system prune -f && docker volume prune -f && docker network prune -f

.PHONY: sk
sk: ## Generates a SK and prints it in the terminal
	@docker build -t ${BACKEND_CONTAINER} -f ./backend/Dockerfile .
	@docker run --rm -d --name ${BACKEND_CONTAINER} ${BACKEND_CONTAINER}
	@docker exec -ti ${BACKEND_CONTAINER} bash -c "echo '+++++ KEY BELOW +++++' && echo 'from django.core.management.utils import get_random_secret_key; print (get_random_secret_key())' |  python manage.py shell && echo '+++++ KEY ABOVE +++++'"
	@docker stop ${BACKEND_CONTAINER}

.PHONY: migrations
migrations: ## Builds the migration files for the database
	@docker exec ${DEV_BACKEND_CONTAINER} python manage.py makemigrations

.PHONY: run-test
run-test: ## Run unit tests
	@docker exec -it ${DEV_BACKEND_CONTAINER} python manage.py test

.PHONY: fixtures-dev
fixtures-dev: ## Populates the DB with fixtures (only use in a dev environment)
	@docker exec ${DEV_BACKEND_CONTAINER} python manage.py loaddata fixtures/dev.json

.PHONY: superuser
superuser: ## Upgrades the user with the given email to a Django superuser
	# Usage: make superuser mail=<your-mail-address>
	@docker exec ${DEV_BACKEND_CONTAINER} python manage.py superuser $(mail)

.PHONY: regularuser
regularuser: ## Downgrades the user with the given email to a user without additional permissions
	# Usage: make regularuser mail=<your-mail-address>
	@docker exec ${DEV_BACKEND_CONTAINER} python manage.py regularuser $(mail)

.PHONY: build-image
build-image: frontend-build ## builds and pushes the container image in the Gitlab registry
	# Usage: make build-image imagetag=1.0.0
	@docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
	@docker build -t ${CI_REGISTRY_IMAGE}:$(imagetag) -f ./backend/Dockerfile .
	@docker push ${CI_REGISTRY_IMAGE}:$(imagetag)

.PHONY: coverage-report
coverage-report: ## Create coverage report of unit tests
	@docker exec ${DEV_BACKEND_CONTAINER} bash -c 'coverage run manage.py test'
	@docker exec ${DEV_BACKEND_CONTAINER} bash -c 'coverage html'

.PHONY: documentation
documentation: ## Starlight Documentation. View on http://localhost:4321/
	@cd documentation && npm run start
