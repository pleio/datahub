# Datahub

A web portal for exploring and interacting with large files.

## Prerequisites:
Get the environment variables:
```sh
cp .env.example .env 
```

## Start the project:
Load the environment variables:
```sh
source .env
```
Start the docker containers:
```sh
make run-dev
```

## Documentation:
This project uses Astro Starlight for the documentation.
Start the Astro server with:
```sh
make documentation
```
Then visit http://localhost:4321/



## License

Copyright 2022 Pleio. Distributed under [this license](LICENSE.md).
