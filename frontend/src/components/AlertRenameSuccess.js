import { useTranslation } from 'react-i18next';
import Spinner from 'react-bootstrap/Spinner';
import Alert from 'react-bootstrap/Alert';

const AlertRenameSuccess = () => {
    const { t } = useTranslation();

    return (
        <Alert key="success" variant="success">
            {t('The file has been renamed successfully.')}
        </Alert>
    );
}

export default AlertRenameSuccess;
