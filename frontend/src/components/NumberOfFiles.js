import React  from 'react';
import { useTranslation } from 'react-i18next';
import Spinner from 'react-bootstrap/Spinner';

const NumberOfFiles = (props) => {
    const { t } = useTranslation();

    return (
        <span className="numberOfFiles">
            {props.value === null ? <Spinner animation="border" size="sm" /> : props.value}
            {' '}
            {props.value === 1 ? t('file') : t('files')}
        </span>
    );
}
  
export default NumberOfFiles;
