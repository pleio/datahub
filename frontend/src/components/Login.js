import { OIDC_URL_LOGIN } from '../const';

const Login = () => {
    window.location.replace(OIDC_URL_LOGIN);
    return null;
}
  
export default Login;
