import { OIDC_URL_LOGOUT } from '../const';

const Logout = () => {
    window.location.replace(OIDC_URL_LOGOUT);
    return null;
}
  
export default Logout;
