import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

// https://www.freecodecamp.org/news/how-to-add-localization-to-your-react-app/
i18n.use(initReactI18next).init({
  fallbackLng: 'en',
  lng: 'en',
  resources: {
    en: {
      translations: require('./locale-en.json')
    },
    nl: {
      translations: require('./locale-nl.json')
    }
  },
  ns: ['translations'],
  defaultNS: 'translations'
});

i18n.languages = ['en', 'nl'];

export default i18n;
