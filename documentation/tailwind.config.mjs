import colors from 'tailwindcss/colors'
import starlightPlugin from '@astrojs/starlight-tailwind'

/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
  theme: {
    extend: {
      colors: {
        accent: {
          50: '#fff0fc',
          100: '#ffe3fb',
          200: '#ffc6f7',
          300: '#ff98ee',
          400: '#ff58df',
          500: '#ff27cc',
          600: '#ff19b6',
          700: '#df008c',
          800: '#b80074',
          900: '#980362',
          950: '#5f0038',
        },
        gray: colors.zinc,
      },
    },
  },
  plugins: [starlightPlugin()],
}
