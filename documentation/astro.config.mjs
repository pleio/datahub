import { defineConfig } from 'astro/config'
import starlight from '@astrojs/starlight'
import tailwind from '@astrojs/tailwind'

// https://astro.build/config
export default defineConfig({
  markdown: {
    shikiConfig: {
      theme: 'dracula-soft',
    },
  },
  integrations: [
    starlight({
      title: 'Datahub',
      logo: {
        src: './src/assets/wonderbit-logo-full.svg',
      },
      sidebar: [
        {
          label: 'Getting Started',
          items: [
            // Each item here is one entry in the navigation menu.
            { label: 'Introduction', link: '/getting-started/intro/' },
            { label: 'Environment Variables', link: '/getting-started/env_vars/' },
            { label: 'Run the software', link: '/getting-started/run_the_software/' },
            { label: 'Git Hooks', link: '/getting-started/git_hooks/' },
          ],
        },
        
        {
          label: 'About the project',
          items: [
            // Each item here is one entry in the navigation menu.
            { label: 'Frontend', link: '/about-the-project/frontend/' },
            { label: 'Database', link: '/about-the-project/database/' },
            { label: 'Licence', link: '/about-the-project/license/' },
            { label: 'REST API', link: '/about-the-project/rest_api/' },
            { label: 'SSO', link: '/about-the-project/sso/' },
            { label: 'Functional Design', link: '/about-the-project/functional_design/' },
            { label: 'Flowchart Renaming File', link: '/about-the-project/flowchart_rename_file/' },
            { label: 'Flowchart Upload File', link: '/about-the-project/flowchart_create_file/' },
            { label: 'Deployment', link: '/about-the-project/deployment/' },
            { label: 'Links', link: '/about-the-project/links/' },
          ],
        },
        {
          label: 'Azure',
          items: [
            { label: 'Azure', link: '/azure/azure/' },
            { label: 'Upload via AzCopy', link: '/azure/azcopy_upload/' },
            { label: 'Add a User', link: '/azure/add_user/' },
            { label: 'Assign Roles', link: '/azure/assign_roles/' },
            { label: 'WORM State Blob Storage', link: '/azure/worm_state/' },
            { label: 'Notification System', link: '/azure/notification_system/' },
            { label: 'Setup Azure Storage', link: '/azure/setup_azure_storage/' },
          ],
        },
        
      ],
      customCss: ['./src/tailwind.css'],
      // markconfig.markdown.shikiConfig.theme
    }),
    tailwind({ applyBaseStyles: false }),
  ],
})
